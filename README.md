# README #
This project contains a kick-off point for "Quotifier" - a training workshop for new Xamarin squads. The Quotifier solution offers wise (and funny) quotes from historical figures through a RESTful API. The project contains a semi-finished version of the API that the reader should clone and use in her own cross-platform project.

- [Who is this document for?](#who-is-this-document-for)
- [What is this repository for?](#what-is-this-repository-for)
- [How do I get set up?](#how-do-i-get-set-up)
- [WARNING! (for PC users)](#warning-for-pc-users)
- [WARNING! (for Mac users)](#warning-for-mac-users)
- [Xamarin Forms](#xamarin-forms)
    - [MVVM](#mvvm)
    - [Why MVVM?](#why-mvvm)
    - [Example](#example)
    - [XAML](#xaml)
    - [Databinding](#databinding)
    - [Custom controls](#custom-controls)
    - [Writing a Xamarin.Forms app](#writing-a-xamarinforms-app)
        - [Android](#android)
        - [iOS](#ios)
    - [Code time!](#code-time)
        - [Android startup](#android-startup)
        - [iOS startup and device provisioning](#ios-startup-and-device-provisioning)
        - [Views](#views)
        - [Wasn't this supposed to be Code time!?](#wasnt-this-supposed-to-be-code-time)
        - [Let's go dynamic](#lets-go-dynamic)
        - [Testing the behavior](#testing-the-behavior)
        - [Live Unit Testing](#live-unit-testing)
        - [Testability and mocking](#testability-and-mocking)
        - [Parameterized tests](#parameterized-tests)
    - [Parallelism](#parallelism)
        - [An asynchronous client](#an-asynchronous-client)
        - [Let's try the app's behavior](#lets-try-the-apps-behavior)
    - [Networking](#networking)
        - [Exploring the API](#exploring-the-api)
        - [Serialization with Newtonsoft.Json](#serialization-with-newtonsoftjson)
        - [Consuming the service](#consuming-the-service)
    - [Let's do it for real](#lets-do-it-for-real)
    - [Done! Now what?](#done-now-what)

### Who is this document for? ###
The reader is assumed to have no experience with Xamarin.Forms and some, but not much, experience with C# and Visual Studio. 

### What is this repository for? ###
* The solution contains three projects: A RESTful API (self-hosted OWIN .NET console), the same Web API suitable for deployment to an IIS server or to Azure. The third project is a Models (shared) project 
* Please note that the API is far from complete and the squads will likely need to change it to suit their design.
* The developers should clone the solution and use it locally, or publish the API to a server; cloud or on premise.

### How do I get set up? ###
To kick off we'll start by getting the mobile backend [from Bitbucket](<https://bitbucket.org/Sanscheval/quotifier.webapi>). To clone it ...

**PC**  
(using cmd or powershell)
> cd \<root folder you prefer to keep your local code repos>  
> git clone https://Sanscheval@bitbucket.org/Sanscheval/quotifier.webapi.git

(Please check out the [WARNING](###WARNING! (for PC users)) below!)

**Mac**  
*(using Terminal)*
> cd \<root folder you prefer to keep your local code repos>  
> git clone https://Sanscheval@bitbucket.org/Sanscheval/quotifier.webapi.git  

Open the solution in Visual Studio (double click the **.sln**) and restore all NuGet packages (right click solution root node -- "**Quotifier.WebApi**" in Solution Explorer and select "Restore NuGet Packages"). 

> - Set the "**Quotifier.WebAPI.ConsoleHost**" project as the start-up ptoject (r-click the project name > Set as StartUp project) 
> - Start the app in debug mode (press `F5`). 
> - Open a web browser and enter this address: <http://localhost:80>.

The browser window should open a default page with links to the API or a swagger UI. The latter is very useful to explore and understand the API. Try getting all quotes and try creating one or two of your own just to familiarize yourself with it. 

<a name="warning-for-pc-users"></a>
### WARNING! (for PC users) ###
The self-hosted console Web API will try and bind to the machine's IP-address, which is necessary for the mobile device emulators to connect to it. In Windows this might cause an Access Violation exception because of the system's security policies. There are two ways to fix this:

1. Run (restart) Visual Studio in elevated mode (start as Administrator)
2. Create URL reservations (urlacl's) to allow non-admin use of IP-based URLs. You do this with netsh, like this (there's a couple of options):  
- Open the command prompt in elevated mode then go ...
- (opt. 1): `netsh http add urlacl url=http://+:7990/ user=tp1\<you>)`
- (opt. 2): `netsh http add urlacl url=http://<machine's ip address>:7990/ user=tp1\<you>)`

While the first netsh option is the most versatile I have found it does not always work (there's still an Access violation being thrown). If so, fall back to option 2. If you still get the exception then just run Visual Studio (or the console app directly, if debugging it isn't required) in elevated mode. 

### WARNING! (for Mac users) ###
On your Mac you might see a "somewhat suboptimal" behavior when you start the app. What happens is that it all starts but the Terminal doesn't actually wait for a input at the `Console.ReadLine()` in the `Program.cs` main method. To fix this just right click the "Quotifier.WebAPI" *project* node, select Options and navigate to Run > Configurations > Default. Then ensure the "Run on external console" and "Pause console output" tick boxes. 

With that, lets dive into Xamarin Forms ...

# Xamarin Forms #
This is a very brief introduction to Xamarin Forms. The intended audience is the mobile ninja that has already 
been introduced into mobile development on the iOS and Android platforms. Some general understanding of common design patterns is also assumed.

<a name="mvvm"></a>
## MVVM ##
Xamarin.Forms provides a very good design pattern out of the box called "MVVM" which is an acronym for Model-View-ViewModel. The design pattern is a modern cousin to the old and tested MVC design pattern. (If you are unfamiliar with the MVC then ... well, go [study!](https://msdn.microsoft.com/en-us/library/ff649643.aspx) :-)) 

Now, let's look at the participating components in the MVVM pattern ... 

**Model**  
The data/entities. There is no difference between a MVVM model and the MVC model.

**View**  
Presenter  
*Examples:*    
`ContentPage` with children, such as ...

- `ListView`  
- `TableView`  
- `Label`, `Button`, `DatePicker` ...  
- ... <https://developer.xamarin.com/guides/xamarin-forms/user-interface/controls/views/>

Elements in views can be laid out using various **Layout controls**, such as ...

- `Grid`
- `StackLayout`
- `RelativeLayout`
- `AbsoluteLayout`

The elements *knows of* its **view model**, which notifies the view of state changes. 

**ViewModel**  
Connects view with model while also controlling the view state. The view model exposes data required by the presenting view (**data binding**). The VM also supports Commands, which can be bound to by elements in the view.

[Here is a good page if you'd like to dig deeper into this pattern.](https://msdn.microsoft.com/en-us/library/hh848246.aspx) 

## Why MVVM? ##
Very strong separation of concern  
Allows for different resources, such as developers and UX designers, to collaborate more easily  
Better code maintenance

<a name="example"></a>
## Example ##
**Model**
```csharp
public class Quote  
{  
    public string Original { get; set; }  
    public string Translated { get; set; }  
    public string[] Categories { get; set; }  
}

public class QuotesRepository  
{  
    public Quote GetRandom() { ... }  
    public IEnumerable<Quote> GetAll() { ... }    
    public void Add(Quote quote) { ... }  
    :  
}
```

**View**  
```xaml
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"   
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"  
             xmlns:vm="clr-namespace:Quotifier.ViewModels;assembly=Quotifier"  
             x:Class="Quotifier.RandomQuotePage"  
             >  
    <ContentPage.BindingContext>  
        <vm:RandomQuotePageVM />  
    </ContentPage.BindingContext>  
    <ContentPage.Content>  
        <Grid>  
            <Grid.RowDefinitions>  
                <RowDefinition Height="*" />  
                <RowDefinition Height="Auto" />  
            </Grid.RowDefinitions>  
            <Label   
                Grid.Row="0"  
                Text="{Binding RandomQuoteText}"   
                Margin="20,0"  
                FontAttributes="Italic" FontSize="30" VerticalTextAlignment="Center" HorizontalTextAlignment="Center"   
                />  
            <Button   
                Grid.Row="1"  
                Text="Refresh"   
                FontSize="35"  
                Margin="0,0,0,20"  
                Command="{Binding RefreshCommand}"   
                />  
        </Grid>  
    </ContentPage.Content>  
</ContentPage>
```

**ViewModel**  
```csharp
class RandomQuotePageVM : ViewModel
{
    private string _randomQuoteText;

    public string RandomQuoteText
    {
        get => _randomQuoteText;
        set { _randomQuoteText = value; OnPropertyChanged(); }
    }

    public ICommand RefreshCommand { get; }

    void onRefreshRandomText() => RandomQuoteText = RemoteQuotesRepository.Singleton.GetRandom().Original;

    public RandomQuotePageVM()
    {
        RefreshCommand = new Command(onRefreshRandomText );
        onRefreshRandomText();
    }
}

class ViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
```

## XAML ##
 The e**X**tensible **A**pplication **M**arkup **L**anguage was originally developed for the Microsoft Windows Presentation Framework (WPF). The language is based on XML notation and provides a flexible and very powerful way of declaring graphical presentation. This way of separating the view declaration from code is an important part of the MVVM concept and allows for easier collaboration and better project agility.

 Scroll back up to [**Example/View**](#example)  to check out a simple example of a view declaration.  

 In the example you see:

 * **Namespace declarations** (in the root tag), such as:  
   `xmlns:vm="clr-namespace:Quotifier.ViewModels;assembly=Quotifier"`  

   This is necessary for the XAML parser to resolve type references, such as on this line:  
   `<vm:RandomQuotePageVM />`  
   The '`vm:`' part is XAML syntax for namespace.

 * **Property assignments** which can be done in two ways:  
   As normal XML attributes, like on this line ...  
   `FontSize="30"`
   ... which is typically fine for atomic/simple values like strings, integers and even some not-too complex non-atomic values like margins. For more complex values, or value graphs, you'll need to encapsulate it into a full XML tag, like this ...   

```xaml
   <ContentPage.BindingContext>  
        <vm:RandomQuotePageVM />  
    </ContentPage.BindingContext>
```

   ... which sets the content page's `BindingContext` property to an instance of the `RandomQuotePageVM` view model class. This example requires for the class to support a public parameter-less constructor of course. Should you find you absolutely need to instantiate with a parameter you can do so by adding an `<x:Arguments>` tag.  

   Let's say there is an alternative constructor for the view model that filters quotes from an author: `public RandomQuotePageVM(string author)`. To pass the name of an author you would have to add it as an argument tag like this:

```xaml
    <ContentPage.BindingContext>
       <vm:RandomQuotePageVM>
           <x:Arguments>
               <x:String>Juluis Caesar</x:String>
           </x:Arguments>
       </vm:RandomQuotePageVM>
   </ContentPage.BindingContext>
```
 * **Graphical elements tree**  
   In the example the tree root in the `ContentPage` which can have a single `Content` element. In this example the content is laid out by a `Grid` control which declares two rows, like so:

```xaml
   <Grid>  
      <Grid.RowDefinitions>  
          <RowDefinition Height="*" />  
          <RowDefinition Height="Auto" />  
      </Grid.RowDefinitions>  
      :
```

   Please note that the grid's first row, which holds the quote `Label`, is set to a `Height` of `*` which means "use whatever's left of your screen estate". The second row, where the button sits, has its `Height` set to `Auto` which simply means it will adjust to accommodate its child(ren).

   The `Grid` control has two children, a `Label` (bound to the VM's `RandomQuoteText` property) and a `Button` (bound to the VM's `RefreshCommand`). The two child controls are laid out into the Grid's two rows by use of its attached `Row` property:  

```xaml
   <Label   
       Grid.Row="0"  
       Text="{Binding RandomQuoteText}"   
       Margin="20,0"  
       FontAttributes="Italic" FontSize="30" VerticalTextAlignment="Center" HorizontalTextAlignment="Center"   
       />  
   <Button   
      Grid.Row="1"  
      Text="Refresh"   
      FontSize="35"  
      Margin="0,0,0,20"  
      Command="{Binding RefreshCommand}"   
      />  
```


## Databinding ##
As we have already seen databinding is key to the MVVM design pattern. For a property to be bound ***to***, in full duplex (ie. to be read and written to), and for the view to be automatically notified of any changes to it, its declaring type must implement the `INotifyPropertyChanged` interface. Also, the property setter must ensure the interface's `OnPropertyChanged` method is called, passing the name of the property, to the implementing interface.  

In the above example all this boilerplate code has been reduced by the use of a special attribute --  `NotifyPropertyChangedInvocator` -- which is declared in the **Annotations.cs** file of the "**Quotatifier.Models**" of the Quotifier.WebAPI solution we cloned earlier. By simply deriving all view model classes from the `ViewModel` base class and ensure the `OnPropertyChanged` method is called by the setter, whenever the property's value changes, the notification mechanism will work. There is no need to pass the property name as a string literal, which helps reduce code coupling greatly.

## Custom controls ##
There is also `BindableProperty`* which are required for all properties of the view that you bind ***from*** and, hence, will have to become a weapon in your arsenal when you find the need to write your own custom graphical controls. For more info on custom controls and attached properties see:  
<https://developer.xamarin.com/guides/xamarin-forms/xaml/bindable-properties/>

*(if you're used to WPF this is the Xamarin.Forms equivalent of `DependencyProperty`)

In Xamarin.Forms XAML files represents an abstraction of the presentation layer of the mobile stack and, so, needs to be able to render any new control you develop in different ways on the mobile platforms. To achieve this you often need to write one *custom renderer* per platform (one for iOS and another for Android).  
For more information on how to write custom renderers see:  
<https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/custom-renderer/>

## Writing a Xamarin.Forms app ##
It is now time to get your hands dirty with writing a '**Quotifier**' cross-platform app. For simplicity we will realize this very small product backlog:

> ***USER STORY 1 / Random quote :***
>
> *As a user I need random quotes when I want them so I can reflect on their wisdom (or be amused).*
>
> ***USER STORY 2 - Random quote*** :
>
> *As the product owner I need a central service to be available in the cloud, so that I can build more tools and add features without having to redeploy or provide my own infrastructure*

 To support this feature we will add a single empty view that contains two elements, a Label and a Button. The label will present the random quote and the button will trigger a refresh that (hopefully) replaces the current quote with a different random one (there is currently no way to prevent the back end to serve up the same random quote again). Maybe here's some room for improvement? ;-) 

 The code for this is already presented (in the above [code examples](##Example)) but let's go through setting up the mobile app solution in Visual Studio on your PC or Mac ...

 **PC**  
*(using Visual Studio 2017)*  
> - Select File > New > Project ...
> - Under the "**Visual C#**" node (under "Installed" templates) select **Cross-Platform**
> - From the available templates select **Cross Platform App (Xamarin)**
> - Set 'Name' to **Quotifier**
> - Ensure the Location is correct. Please note that Visual Studio will create a sub folder by the name you just specified (**Quotifier**). I my case the solution will end up in `d:\Source\Quotifier`
> - Tick the **Create new Git repository** (it's always a good idea to version control the code, even locally)
> - Click **OK**

![Create application dialog](images/createSolutionDialog1.png)

You will now be presented with a final set of options:

![Create application options](images/createSolutionDialog2.png)

> - Ensure the **Xamarin.Forms** option is ticked (under **UI Technology**)
> - Also ensure **Shared Project** is selected (under **Code Sharing Strategy**). See **[A few words on "libraries"](#awordonlibraries)** (below) for more information on this topic.
> - Click **OK**

Visual studio will now stage a default Xamarin.Forms mobile solution. The solution includes four projects:

> - A Shared library named **Quotifier**
> - An Android application named **Quotifier.Android**
> - An iOS application named **Quotifier.iOS**
> - A UWP application named **Quotifier.UWP**

Just before Visual Studio gets ready to give back control you will be presented with one or two final dialogs, both related to the UWP project ...

![WUP dialog 1](images/createSolutionUwpDialog1.png)

> - Just click **OK** and/or close the dialog(s), accepting the default options.

*We won't be bothering with the UWP (Universal Windows Application) in this excersice so when all is set up and Visual Studio is ready to continue, go ahead and remove the UWP project if you like.*

> - Before we continue, let's restore all NuGet packages first by right-clicking the solution node and select **Restore NuGet packages**

We are now ready to try out the app. Of course, it won't be supporting the product backlog (fetch and present a random quote) but let's just ensure it runs before we continue.

### Android ###
> - To test the Android project in an Android emulator on your PC, go ahead and right click the **Quotifier.Android** project and select "Set as StartUp project".
> - Next, check the toolbar at the top of Visual Studio. There should be a drop down allowing you to select an Android emulator:

![Select Android emulator](images/selectAndroidEmulator.png)

> - To start the app just click the emulator or press `F5` on the keyboard ...

Please note that the first time you build the app, especially the Android app, it will take some time as Visual Studio starts by restoring NuGet packages, compiles, links, starts the emulator and then deploys the app to it. Expect several minutes for this to complete. Also, as of now, it is not uncommon that the process gets stuck on the last step. If that happens you can select **Build** > **Cancel** and retry.

If all goes well you should now see a very simplistic app running in the emulator:

![Hello Xamarin.Forms](images/androidEmulator.png)

The tool bar sitting to the right can be used to emulate interaction, such as rotating the device, go to home screen and so on.

> - To stop the run return to Visual Studio and click the little red icon at the top (or press `SHIFT+F5`):

![Stop run](images/vsStopRun.png)

*Hint: the flanking icons up there are (to the left) Pause and (to the right) Restart*

### iOS ###
To build and run the iOS app in an iOS emulator you need to be connected to a **Xamarin build agent**, meaning you need to be connected to a Mac that has Xamarin installed and that is available on your network. 

CAUTION! The following section might cause moments of frustration so feel free to think up a few useful profanities and move on (just don't say you weren't warned) ... 

> - To test the iOS project start by setting it as the StartUp project. You can do this like with the Android project (see above). Another, faster, option is to select the iOS project in the drop down from the Visual Studio toolbar: 

![Select platform project](images/selectPlatformProject.png)

All Apple software must be built with Macs. Visual Studio solves this by installing a build agent called Xamarin agent on your Mac when you install Visual Studio on it. In Visual Studio the Xamarin agent is represented by this little icon:

![Xamarin build agent](images/xamarinAgentIcon.png)

In the above example the icon indicates it is **NOT** connected to the Xamarin agent. The icon will turn green once it has connected but in this case we need to help the Studio find and connect to the agent ... 

> - Click the Xamarin agent icon.

![Xamarin build agent](images/xamarinAgentDialog1.png)

> - Go through the wizard to get the Mac configured and make the Xamarin agent available.
> - Click **Next**
> - Assuming this is the first time you connect to the Xamarin agent click **Add server**

![Add server](images/xamarinAgentAddServer.png)

> - Type the name of your Mac, or its IP address (the name is of course more convenient in a DHCP setup)

![Logon to Xamarin agent](images/xamarinAgentLogon.png)

> - Log on to the Xamarin agent using the same credentials you use for the user currently logged on to he Mac (typically your AD credentials)

*If you are unsure of your actual user name on the Mac then, on your Mac, open a terminal, type "whoami" and check the result*

![Connecting to Xamarin agent](images/xamarinAgentConnecting.png)

If you run into networking or authentication issues at this point [please check this page for more information](https://developer.xamarin.com/guides/ios/getting_started/installation/windows/connecting-to-mac/visual-studio-ssh/) (the page also contains a troubleshooting link).

Hopefully the connection was successful and your profanities were left unuttered. If so the Xamarin agent icon should now be set to green:

![Connected to Xamarin agent](images/xamarinAgentConnected.png)

With that let's test-run the iOS app ...

> - Ensure the "Solution platforms" option is set to "iPhoneSimulator"
> - Select a suitable simulator and iOS version. Let's go with an iPhone 8 and iOS 11.1: 

![iOS target platform](images/iosTargetPlatform.png)

> - Hit `F5` or click the green "Run" arrow, just like when we tested the Android app before

Visual studio now connects to the Xamarin agent on the Mac to build the app. Usually this process is much faster than for Android. The Studio then launches a simulator proxy to be run locally (the simulator is actually executing on your Mac). If all goes well you should see this:

![iOS simulator](images/iosSimlatorHelloXamarin.png)

**Well done!** 

Now, lean back and enjoy your success for a moment. When boredom finally succumbs to ambition return to Visual Studio and stop the run by clicking the little red icon at the top (or press `SHIFT+F5`) so we can implement that backlog! 

## Code time! ##

Before we dive into the code cave and start enjoying ourselves let's just go through what's been set up for us so far ...

Like we saw before there's three projects that will build executable apps, for Android, iOS and Windows. The third one (Quotifier.UWP) will be ignored for now and you can even remove it if you haven't already. (I usually dump it to get rid of the annoying configuration dialogs it throws in our face every time we load the solution.) All apps share a common class library; "Quotifier". Ideally all code will end up in that library but reality sometimes dictate that you'll need to do write some platform-specific implementations to satisfy your needy product owner. To succeed in that you will need knowledge about the mobile platforms of course but that's outside the scope of this simple exercise.

### Android startup ###

In Android there needs to be a "main activity" as the entry point. The studio was kind enough to create a class with that name (well, almost) in a `MainActivity.cs` file. Go ahead and double click it to see what's in there. 

![Android MainActivity](images/droidMainActivity.png)

Basically, there's a few lines of boilerplate code before Xamarin.Forms gets initialized. The app then loads the application passing an instance of the `App` class. That class is implemented in the shared library, in the `App.xaml` and `App.xaml.cs` files.

> - Open the `App.xaml.cs` file 

*HINT: if you're too lazy to go find that file you can right-click the `App` identifier and select "Go To Definition"*.  

![Shared App class](images/sharedApp.png)

Again, not a lot going on here. The `App` class ctor (short for 'constructor') calls its own `InitializeComponent` method before it creates the page you've been seeing in the emulators earlier and assigns it to its `MainPage` property. Should you like for the app to use a different page as its root view this is the line you'd like to change. 

The other methods represent the app's life cycle and contain only comments. You can remove them but those methods were scaffolded to remind us we might might want to put some code in there, reacting to how he user passes the app through its life cycle. Typically, you might want to load some configuration in the `OnStart` method and then stow away stuff when the app is being put to sleep, only to be restored again when it's brought back again. Remember you are writing mobile apps here, to be run on a pocket computer with limited amount of resources. To deal with this iOS and Android might delete stuff you put in memory so you can't expect it to still be there when the user flips back to you app after having briefly checked out her Twitter or Facebook part of reality.

> - To check out how the app life cycle works go ahead and place a breakpoint in each of the `OnStart`, `OnSleep` and `OnResume` methods (place the cursor on a line and press `F9`, or click the editor's left margin (see screen shot above). 
> - Run the app (`F5`). After a short while the debugger should break inside the `OnStart` method. 
> - Resume the session (press `F5` again, or click the "Continue" button in the toolbar if you're one of those "mousey" coders out there :-) ) 
> - The splash screen will show for a while and then we're seeing the main page and it's warm welcome message again.
> - Emulate pressing the HOME button on the device (the emulator's circle-shaped tool). The debugger now breaks in the `OnSleep` method. 
> - Resume the session again (`F5`). If you like you can flip to some different app.
> - Flip back to our 'Quotifier' app again and you will now see the debugger breaking in the `OnResume` method. 

*For more information on app lifecycle, [have a look here](https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/app-lifecycle/)*

*For more information on how to persist your stuff, [check this out](https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/application-class/#Persistence)*

### iOS startup and device provisioning ###

The startup for iOS is very similar to Android and the same principles apply. Life for an iOS app begins in the `AppDelegate.FinishedLaunching` much the same way as you saw in the Android app. A few lines of initializing Xamarin.Forms and then loading an instance of the shared `App` class.

If you want to "sideload" your app to an iPhone or iPad there are a few things you need to arrange first. The iOS is not open source, like Android, and the infrastructure is closely guarded by Apple. Apple is renown for its great design and how to make complicated things appear very easy. Alas, that philosophy is for end consumers only. For developers, Apple has not only done the opposite (the apparently simple become complicated); the fine company has taken "complicated" and elevated it into a fine art! 

Device provisioning is certainly a chapter in its own right and I won't be going into details here. Instead, if you'd like to try running your app on a physical device (without releasing it to Apple's AppStore or Tetra Pak's app catalogue) [I recommend you check out this page](https://developer.xamarin.com/guides/ios/getting_started/installation/device_provisioning/). Also, [here's a great explanation of all device provisioning terms and how it all works](http://sharpmobilecode.com/making-sense-of-ios-provisioning/). 

### Views ###

Let's move on to the `MainPage`. This baby is also partially declared with a **XAML** file (`MainPage.xaml`) and a code-behind file (`MainPage.xaml.cs`). 

> - Navigate to the  `MainPage.xaml.cs` file.

![MainPage code behind](images/sharedMainPageXaml-01.png)

The class derives from the `ContentPage`, which is typically what you'd like. What's important to remember is that the `ContentPage` has a `Content` property which is where you put the -- you guessed it -- content of the page. There are other types of pages of course. [Have a look here to check them out](https://developer.xamarin.com/guides/xamarin-forms/user-interface/controls/pages/). 

The `MainPage` ctor contains a single line of code: `InitializeComponent()`. This call is needed to start the rendering process, laying out all visual elements as described in the **XAML** file - `MainPage.xaml`.

*Please note that using XAML to declare pages and views is actually optional. There is nothing stopping us from not using XAML and just code everything instead. That, however, would break the best practice established by the MVVM pattern depriving us both from good separation of concern and the ability to work with the visual presentation as "assets", perhaps created and maintained by UI designers rather than us coders. That last part is currently difficult, seeing that there is no good design tool to support Xamarin's flavor of XAML. But that is likely something that will change and by utilizing XAML we will be in a good place when our UI people can get their hands on tools like that in the future.*  

> - Open the `MainPage.xaml` file.

![MainPage code behind](images/sharedMainPageXaml-02.png)

As you're probably noticing the XAML specifies a `ContentPage` as its root tag. The tag declares two `xmlns` (XML namespace) attributes that are needed by XAML parser/renderer (the two URLs) and one `local` that might come in handy. The last attribute (`x:Class="Quotifier.MainPage"`) is what connects the XAML declaration to the actual class we just examined. Changing that line, or renaming the `MainPage` class without modifying the XML attribute, will of course break this connection (and cause the uttering of some profanities again). So, if you find the need (and you will) to rename the page, please use the built-in refactoring tools to make your life more enjoyable. Let's try that ...

> - Go back to the `MainPage.xaml.cs` file again.
> - Right-click the class identifier ("`MainPage`") and select "Rename".

![Renaming MainPage](images/sharedMainPageXaml-rename.png)

The Studio will now try and help with the renaming, to avoid disconnecting code that needs to remain connected. 

> - Tick all three options in the dialog ("Include comments", "Include strings" and "Preview changes")
> - Stick a "My" in front of "MainPage" as the class' new prefix, to make it `MyMainPage`.
> - Click **Apply**

![Renaming MainPage](images/sharedMainPageXaml-rename-preview.png)

This is the time we spend a few minutes silently sending thoughts of appreciation to the good folks over at Microsoft for providing all the automagic needed to make Xamarin work! There are loads of behind-the-scenes files (well, virtual files really) that contain boilerplate code we can't even see and that, when they break or gets corrupted, we will have a very bad day at the office. Endless cups of coffee will be drunk (followed by many many trips to the men's or ladies' room), variations of "WTF" will be heard repeatedly and your web browser will consume billions of clock cycles as you roam every forum out there desperately hunting for answers and solutions. 

To summarize: **Never. Ever. Rename. Yourself.**

> - Click **Apply**

Other than all those virtual files the Studio will now have modified these files to reflect the new class name:

`MainPage.xaml.cs`, `MainPage.xaml`, `App.xaml.cs`

This is just from having staged a nonsense one-page app. Imaging a few sprints into the project, when there's a lot of pages that (typically) renders more than just a label saying "Welcome to Xamarin.Forms!".  

> - Open any text editor and create a new page
> - Write (don't copy) this twenty times: "**Never. Rename. Yourself.**"
> - Just kidding. :-)

### Wasn't this supposed to be Code time!? ###

Right. Sorry. Enuff exploring. Let's get dirty with some code.

Remember the backlog supposed to realize? No? Here it is again:

> ***USER STORY 1 / Random quote :***
>
> *As a user I need random quotes when I want them so I can reflect on their wisdom (or be amused).*
>
> ***USER STORY 2 - Random quote*** :
>
> *As the product owner I need a central service to be available in the cloud, so that I can build more tools and add features without having to redeploy or provide my own infrastructure*

Basically we need a single page (like `MyMainPage`) with a label to show a random quote and a button to tap when the user wants another random quote.

Like I said before, a `ContentPage` contains one `Content` property that can be assigned a single `View` object. Luckily all graphical elements in Xamarin derives from `View` and, so, can be used for content. As we need *two* UI elements (a Label and a Button) we need to assign the `Content` some UI element that in turn can hold multiple UI elements. 

This is where **Layout**s come into play. Xamarin.Forms supports several different layouts, such as `StackLayout`, `Grid`, `RelativeLayout`, [and more](https://developer.xamarin.com/guides/xamarin-forms/user-interface/controls/layouts/) and they are all are used, as their names imply, to layout other UI elements. Basically, a `StackLayout` just stacks its child elements on top of each other or next to each other (controller by its `Orientation` property). You can also use layout controls as children to other layout controls to create fluid and good looking views. For example, you could stack several horisontally oriented stack layouts on top of each other in a vertically oriented StackLayout. Doing so, however, might come with a performance penalty. As your app kicks into rendering mode the XAML you composed will be used to create (and compile) native elements that embed other native elements. If you create deep composiotions with layers upon layers of powerful Xamarin.Forms controls you risk ending up with even deeper graphs of native controls and a layout engine that is gasping for air as it works overtime to keep up. The end result might be a sluggish experience and disheartened users. Again, remember we are not targeting a powerful desktop box here.    

Other layouts, like **RelativeLayout** are more difficult to get to grips with but offer more powerful layout scenarios where you specify exactly how its child elements should relate to each other (and to the layout itself), horizontally and vertically.

Let's start simple with a `StackLayout` and see what that gives us.

> - Navigate back to the `MainPage.xaml` file

As you can see there's already a `Label` in that page and it´s a child of a `StackLayout`. The label's `VerticalOptions` and `HorizontalOptions` is what positions it at the center of the layout (and screen). As with most UI controls there's a lot of properties you need to know to make things look sweet. For `Label` you can set its `FontFamily` (like "**Segoe UI**", or Tetra Pak's official one: "**Avenir**"), `FontSize`, `FontColor` or `FontAttributes` ("**Bold**" or "**Italic**"). Currently the Label's `Text` is set to "**Welcome to Xamarin.Forms!**" so we need to somehow change that to the random quote. Before we do that, let's add a `Button` and make it say "Refresh":

> - Add a `Button` tag after the `Label`: `<Button Text="Refresh" />`

```xaml
    <?xml version="1.0" encoding="utf-8" ?>
    <ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
                 xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                 x:Class="Quotifier.MyMainPage">
        <ContentPage.Content>
            <StackLayout>
                <Label 
                    Text="Welcome to Xamarin.Forms!"
                    VerticalOptions="CenterAndExpand" 
                    HorizontalOptions="CenterAndExpand" />
                <Button 
                    Text="Refresh"/>
            </StackLayout>
        </ContentPage.Content>
    </ContentPage>
```

> - Run the app

![Renaming MainPage](images/helloXamarin-button.png)

Since this is supposed to be a quotes app we should probably make it look nicer.

> - Experiment with background colors (set the `ContentPage`'s `BackgroundColor` attribute), font family, text color and so on to make 'Quotifier' look more serene and suitable for conveying wisdom. 

[Here's a nice page with all fonts supported by iOS](http://iosfonts.com/). Knock yourself out!

How about this (iPhone):

![Renaming MainPage](images/iosHelloXamarin-black.png)

For this I used:

**Label:**

Background: **Black**; Font family: **Cochin**; Font size: **20**; Font attribute: **Italic**; Text color: **AntiqueWhite**

**Button:**

Text color: DarkGray

> - Run the app on Android ...

![Renaming MainPage](images/droidHelloXamarin-black.png)

Seems we're in trouble here. Apparently the Cochin font isn't supported by Android and the text difficult to see against the default Android button background. 

For Android there are only three default fonts: "**normal**" (Droid Sans), "**serif**" (Droid Serif), and "**monospace**". Now, XAML is supposed to be run both on Android and iOS so this means we have now stumbled upon our first x-platform problem: How to deal with platform-specific differences while sharing XAML declaration? For this situation there is a special XAML tag to the rescue: `<OnPlatform>`. Being a tag itself you need to use the tag approach for setting the attributes of other tags. So, instead of saying `FontFamily="Cochin"` or "`FontFamily="serif"`" you need to make the assignmend a sub tag: `<Label.FontFamily>Cochin</Label.FontFamily>` (iOS) and `<Label.FontFamily>serif</Label.FontFamily>` (Android). 

Of course, just replacing the assignment from its attribute form to its tag form won't solve our problem in itself. We need to use the <OnPlatform> tag as the value, like this:   

```xaml
    <Label 
        Text="Welcome to Xamarin.Forms!"
        FontSize="20"
        TextColor="AntiqueWhite"
        FontAttributes="Italic"
        VerticalOptions="CenterAndExpand" 
        HorizontalOptions="CenterAndExpand"
        >
        <Label.FontFamily>
            <OnPlatform x:TypeArguments="x:String">
                <On Platform="iOS">Cochin</On>
                <On Platform="Android">serif</On>
            </OnPlatform>
        </Label.FontFamily>
    </Label>
```

Please note the `x:TypeArguments` attribute in the `<OnPlatform>` tag. This is needed to ensure type safety in XAML.

Before we run it again, here's yet another option to instruct the Studio which app we want to run. In this case it would be nice to run both apps at the same time. 

*Running two mobile apps is usually fine but you might run into "peculiarities" when you perform step-by-step debugging with Visual Studio 2017. Be warned.*

> - Right-click the solution root node. The one at the top of the Solution Explorer saying "Solution 'Quotifier' (4 projects)
> - From the context menu; select "Set StartUp Projects..."

![Renaming MainPage](images/vsSetStartupProjects.png)

> - Tick the "Multiple startup projects" radio button
> - Drop down the menu for Quotifier.Android and select "Start"
> - Drop down the menu for Quotifier.iOS and select "Start"
> - Click **OK**
> - Run the app ...

![Renaming MainPage](images/helloXamarin-fonts.png)

Seems that did the trick for the text. The button still looks like it needs some love though.

* Using the `<OnPlatform>` tag, fix the problem with the button text color (hint: the `x:TypeArguments` value should be "**Color**").

### Let's go dynamic ###

With that we're done with the static part of the app. Next, we will add dynamic data (fetching the random quote) and add interaction (fetching a new random quote when user taps the "**Refresh**" button).

[Like we discussed earlier, we aim to implement the MVVM pattern](#mvvm). This means we will now introduce a **view model** to drive the view. 

> - Create a new folder in the shared ("**Quotifier**") project by right-clicking its root node and select Add > New Folder
> - Name the folder "**ViewModels**" and save it
> - Copy the "**Annotations.cs**" and "**ViewModel.cs**" files from the shared "**Quotifier.Models**" project in the "**Quotifier.WebAPI**" solution into the new "ViewModels" folder. This will save us some trouble.
> - Create a new class in the new "**ViewModels**" folder by r-clicking it and select Add > Class...
> - Name the class "**RandomQuoteVM**" (VM = view model; let's try and stay sane here) and click **Add**
> - Modify the new `RandomQuoteVM` and make it derive from `ViewModel`
> - Add a `string` property with a backing field, name it "**RandomQuote**" and make the setter call the `ViewModel`'s `OnPropertyChanged` method to ensure XAML's binding mechanism gets notified of any changes.
> - Add a command property (type: `ICommand`) and name it "**RefreshCommand**". Just provide a getter (commands usually aren't replaced in runtime).
> - Initialize the "**RandomQuote**" property with something to indicate the app is busy fetching a random quote. 

This is what the class view model could look like so far:

```csharp
    class RandomQuoteVM : ViewModel
    {
        private string _randomQuote;

        public string RandomQuote
        {
            get => _randomQuote;
            set { _randomQuote = value; OnPropertyChanged(); }
        } 

        public ICommand RefreshCommand { get; }

        public RandomQuoteVM()
        {
            _randomQuote = "(please wait ...)";
        }
    }
```

The next step will be to bind the view to its view model. You assign the view its view model by assigning its `BindingContext`. You can do this using code or XAML, or both. Personally, I like using XAML as that also gives the Studio's XAML editor a chance to assist me with binding to the view model. To make that work we need to help XAML find the type by adding a new XML namespace. let's call it "**viewModels**" for simplicity.

> - Open the `MainPage.xaml` file and add a new **xmlns** attribute:  `xmlns:viewModels="clr-namespace:Quotifier.ViewModels"`
> - Assign an instance of the `RandomQuoteVM` class to the `ContentPage.BindingContext` property using the tag syntax.

This is what the view can look like so far:

```xaml
    <?xml version="1.0" encoding="utf-8" ?>
    <ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
                 xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                 xmlns:local="clr-namespace:Quotifier"
                 xmlns:viewModels="clr-namespace:Quotifier.ViewModels"
                 x:Class="Quotifier.MyMainPage"
                 BackgroundColor="Black"
                 >
        <ContentPage.BindingContext>
            <viewModels:RandomQuoteVM/>
        </ContentPage.BindingContext>
        <ContentPage.Content>
            <StackLayout>
                <Label 
                    Text="Welcome to Xamarin.Forms!"
                    FontSize="20"
                    TextColor="AntiqueWhite"
                    FontAttributes="Italic"
                    VerticalOptions="CenterAndExpand" 
                    HorizontalOptions="CenterAndExpand"
                    >
                    <Label.FontFamily>
                        <OnPlatform x:TypeArguments="x:String">
                            <On Platform="iOS">Cochin</On>
                            <On Platform="Android">serif</On>
                        </OnPlatform>
                    </Label.FontFamily>
                </Label>
                <Button 
                    Text="Refresh" 
                    >
                    <Button.TextColor>
                        <OnPlatform x:TypeArguments="Color">
                            <On Platform="iOS">LightGray</On>
                            <On Platform="Android">DarkGray</On>
                        </OnPlatform>
                    </Button.TextColor>
                </Button>
            </StackLayout>
        </ContentPage.Content>
    </ContentPage>
```

Finally, let's replace that tiresome old "**Hello Xamarin.Forms!**" with whatever the view models serves up:    
​    
> - Bind the Label's Text property to the view model's "**RandomQuote**" property and make it one-way (read only): `Text="{Binding RandomQuote, Mode=OneWay}"`

*If you feel the urge to know more about binding in general and binding mode in particular; [check out this page](https://developer.xamarin.com/guides/xamarin-forms/xaml/xaml-basics/data_binding_basics/#The_Binding_Mode)*. 

> - Bind the Button's `Command` to the view model's `RefreshCommand` property: `Command="{Binding RefreshCommand}"`

Now everything is wired up to make the view dynamic and interactive. This means we should start working on the view model and wire it up to the layers and units needed to actually fetch random quotes whenever it suits the user. But first, let's make sure the wiring so far is transmitting signals like we hope it is.

> - Flip back to the `RandomQuoteVM` class and instantiate the command from the ctor. For the `Command` ctor provide a pragma method called **refresh**: `RefreshCommand = new Command(() => refresh());`
> - Add the **refresh** method and make it private. You can leave the body empty.

The class should now look like something close to this:

```csharp
    class RandomQuoteVM : ViewModel
    {
        string _randomQuote;

        public string RandomQuote
        {
            get => _randomQuote;
            set { _randomQuote = value; OnPropertyChanged(); }
        }

        public ICommand RefreshCommand { get; }

        void refresh()
        {
        }

        public RandomQuoteVM()
        {
            _randomQuote = "(please wait ...)";
            RefreshCommand = new Command(() => refresh());
        }
    }
```

> - Add a breakpoint inside the **refresh** method and start the app ... 

![Please wait ...](images/pleaseWait.png)

OK, looks like the text binding works. 

> - Tap the "Refresh" button

With any luck the debugger will now break as expected in the `RandomQuoteVM.refresh` method. We now need a component that can act as a client for our back-end and actually fetch that random quote. We are hereby leaving the view layer to enter the view model, model (and beyond) of our architecture. Most of that will be coding.

But before we start our descent for landing this baby (yes, I absolutely love aviation) let's be good little coders and add tests to ensure the app behaves as expected. Ever heard about Test Driven Development (TDD)? Here it comes ...

### Testing the behavior ###

> - Add a new shared project to act as the communications (client) layer: r-click solution node > Add > New project... > Visual C# > Search: shared > Shared Project > Name:  "**Quotifier.Client**" > click **Add**  
> - Add a new interface to the new shared Quotifier.Client project. Call it "**IQuotifierClient**": r-click project node > Add > New Item... > Visual C# > Code > Interface > Name: "**IQuotifierClient.cs**" > click **Add**
> - We now need a unit test project:  Add > New Project... > .NET Core > xUnit Test Project (.NET Core)
> - Name the new test project "**Quotifier.Tests**" 
> - Rename the default "**UnitTest1**" class to "**TestQuotifierClient**"
> - Add a reference to the new shared **Quotifier.Client** project: r-click test project's **Dependencies** node > Add Reference... > Shared Projects > tick "**Quotifier.Client**" > Click **OK**

Now, if you're unused to test driven development (TDD) this might feel weird but from now on we will drive all new code from tests. That basically means we always start by writing tests for whatever feature we intend to implement, wherever feasible. It is usually difficult to "unit test" the presentation layer, as that usually requires simulating user interactions such as typing and maneuvering the mouse. But most other component's development and behavior should be driven from the code that tests them; hence the term "Test-Driven Development". If you feel a sense of vertigo and that the world is "askew" it's because you haven't gotten used to it yet. But in no time, once you've gotten used to TDD, you will never go back. Trust me! 

In short, your Test-Driven Development is an iterative process, like this:

![TDD flowchart](images/tdd-flowchart.png)

1. Write some tests for the new feature you about to realize.
2. Run those tests and make sure they fail (they should since none of the tested features are actually implemented).
3. Next, implement the tested behavior (the "units" being tested).
4. If you're a real ninja then all tests now pass. If you're not quite the ninja, but maybe more like karate-kid, then some tests will still fail. Then repeat step 3 and try again until all tests pass.
5. You are now free to refactor your code, and clean it up, knowing well that if something breaks your tests will alert you. This will leave you with a sense of freedom to move on, adding features and complexity as you go. Your Product Owner will love you. Your squad mates will respect you. Your dog will wag its tail more fervently and your karma will sky-rocket. 

Also, the Studio will make life easier by helping us generate the basis for the tested code, as you will see.

As for the new test project we just created; xUnit is one of many different unit test frameworks out there, such as NUnit or MSTest. I have found that xUnit is a good fit for cross-platform mobile development so that's the reason I selected it for this exercise. If you have never seen it before let's just say that all tests are implemented as methods with a `[Fact]` attribute, strangely enough (one could argue with some plausability that a "**Test**" attribute would make more sense but there you go). The method also needs to be `public` and should not return a value (so `void`). This enables Visual Studio's (and MSBuild's) testrunner to find the tests and execute them, when we trigger testing from within the Studio but also later, in automated continous integration and continous deployment (CI/CD) flows. 

The first test will just try and fetch a new random quote from the client, which still doesn't exist. To ensure it actually gets a *random* quote we'll fetch several quotes and check that at least one is different from the last. Let's do this ...

> - Rename the staged "**Test1**" method of the test to "**TestGetRandomQuote**"
> - Write the test, like this:

![test get random quote](images/testGetRandomQuote.png)

Unsurprisingly, Visual Studio has underlined two identifiers: the (local) `getClient` method and the `GetRandomQuote` method of the `IQuotifierClient` interface. The reason is, of course, none of them exists. (This is where TDD can feel a bit "backwards".) Luckily, the Studio will help us fix that in an iffy ...

> - Right-click the `getClient` identifier > Quick Actions and Refactorings... > Generate method '**TestQuotifierClient.getClient**'

The Studio now implements the method by throwing a `NotImplementedException`. That's fine for now.

> - Generate the "**GetRandomQuote**" method the same way. Please note that the method will be added to the `IQuotifierClient` interface.

The code is now in a shape to be built so we can move on to the second TDD phase: "See Tests Fail".

> - Test > Run > All Tests

The Studio builds the solution and runs all tests it can find. As of now there should be only one. The result is presented in the "Test Explorer" pane:

![test explorer](images/testExplorer.png)

> - Click the failed test and check the result.

Looks like the test fails like we expected. When it calls the `getClient` method a `NotImplementedException` is thrown. Sweet!

### Live Unit Testing ###

*Please note that this feature only works with Visual Studio **Enterprise** edition. If you are running **Professional** or **Community** please skip ahead.*

Visual Studio Enterprise offers "Live Unit Testing" if you have the clock cycles to spare. Live Unit Testing means the Studio will be running unit tests automatically in the background as you add or modify your code. Rather than running all existing unit tests over and over (draining your box's resources) it will apply some silicon-based intelligence and  analyze which tests needs to be run, based on what part of the code you are modifying. This is great for your "test awareness" and your sense for code quality!  

> [OPTIONAL]
>
> - To activate Live Unit Testing go: Test > Live Unit Testing > Start
> - To stop Live Unit Testing go: Test > Live Unit Testing > Stop

![Live unit testing](images/liveUnitTesting.png)

With Live Unit Testing the Studio will indicate the current state of your code directly in the editor (see above screenie). In this example you can see that the test has failed and also on which line it failed (the red cross). Another convenient feature of Live Unit Testing is that you can click the indicator to get a context menu. In the menu you will see all tests running this code. In this case the code *is* the test so there is just one test presented. If you select the test you will be presented with some more options that allow running or debugging that test directly.    

> [OPTIONAL]
>
> - Add a breakpoint on test's leading curly brace
> - Click the little lab flask (or red cross) in the test to get a menu
> - In the menu; select the test (marked with a red cross to indicate it has failed)
> - Select "**Debug Selected**"
>
> The Studio now starts a debug session running just the test:
>
> ![Live unit testing](images/liveUnitTesting-debug.png)
> - To step through the code one line at a time press `F10`
>
> ![Live unit testing](images/liveUnitTesting-debugError.png)
> Visual Studio presents the exception. You can click the **View Details** link to analyze the error in detail, including inner exceptions, stack trace and more.
>
> - Stop the debugging session like you stop the app; using the red "stop" tool or press `SHIFT+F5`.

### Testability and mocking ###

We now have a test for our intended "random quote" feature and we have confirmed that it fails (no false negatives, please). So, time to move on to step three of the TDD cycle - "Write Code" - to produce a random quote. Let's begin where it now fails ...

By the way! You might have heard about "Behavior Driven Development" and you might now ask yourself if there are any similarities with TDD? The short answer is "yes, it's TDD with a twist".

BDD is about **how** you approach TDD. Basically, BDD makes sure you are testing the *behavior* of your components, making no assumptions on how they are *implemented*. One simple example is if you are testing to make sure incrementing a "counter" is indeed returning an *incremented* value; your test could easily be written to just create a counter and call its `increment()` method and then assert it returns the value one (1). If you wrote the counter yourself, and you know it's internal value is always zero after initialization, then testing for a specific vaue  would appear to make sense.  

But such a test would effectively be testing the unit's implementation. There are also tools and theories on how you can express a behavior and have it drive testing and implementation but let's not worry about that right now. There are plenty of knowledge on the web if you are interested in BDD.      

Now, the `getClient` method is responsible for creating and returning an object that implements the `IQuotifierClient` interface. But why an interface, and not a class? The consuming end of the client we are about to write only cares about being able to get a random quote. It should not care *how* that random quote gets generated or where it comes from (sounds familiar?). Relying on an interface is like relying on a behavior, not the implementation, which allows us the freedom to write the implementation any way we like. This is great for *forcing* us not to introduce any dependencies between consumer and implementor. It is also great for testing and mocking ...

Testing a client that is implemented to call a remote service makes the test dependent on the fact this service is actually running. You could of course start the provided RESTful **Quotifier.WebAPI** in a console window and the start then test. This is certainly something we want to test, eventually, but as that introduces a dependency to a remote service the app behavior cannot be tested without the service. That's not good, and this is one reason we opted to write an interface (a behavior or *agreement* for a behavior) for the client.     

Armed with our interface we can now go ahead and write a "*mocked*" implementor of the client, just to test the behavior. The mocked client can be written to serve up quotes like expected or get stuck serving up the same quote over and over. This approach allows us to test the behavior for failures as well as green field scenarios. 

Let's write a mocked client ...

> - Add a new folder to the **Quotifier.Client** project and name it `Mocking`
> - Add a new class to the new folder and name it `MockedClient`
> - Make the class implement the `IQuotifierClient` interface:

![Mocked client](images/mockedClient-01.png)

Visual Studio has underlined the interface identifier to indicate it is not fully implemented by the class. There is also a quick-fix available, indicated by the yellow light bulb in the left margin.

> - Click the yellow quick-fix light bulb and select "**Implement Interface**"
> - To test when randomization is broken add a `bool` property called `IsRandomBroken`
> - Remove the line throwing an exception and replace it with code to randomly return one of five mocked quotes.

I would encourage you code it yourself but if you're lazy or short on time here's a suggestion:

```csharp
    class MockedClient : IQuotifierClient
    {
        Random _random = new Random(DateTime.Now.Millisecond);
        string[] _quotes = new[] {
            "Women are made to be loved, not understood",
            "From now on, ending a sentence with a preposition is something up with which I will not put.",
            "Anyone who lives within their means suffers from a lack of imagination.",
            "Genius is born--not paid.",
            "Veni vidi vici"
        };

        public bool IsRandomBroken { get; set; }

        public string GetRandomQuote()
        {
            return IsRandomBroken
                ? _quotes[0]
                : _quotes[_random.Next(5)];
        }
    }
```

Save it and check out the test.

![Mocked client](images/testGetRandomQuote-success.png)

If you still have Live Testing running, shortly after you saved the mocked client you will see the test has passed, as indicated by the green check marks in the above screenie. You can also return to the Test Explorer and run the test. The result should now look like this:

![Mocked client](images/testGetRandomQuote-success-02.png)

Now, let's write a test to ensure we can deal with a client that fails at delivering a random quote. To do so we need a way for `getClient` to set the `MockedClient.IsRandomBroken` property to true. 

> - Add a `bool` parameter to the `getClient` method and call is `isRandomBroken`
> - Modify the `getClient` method so that is assigns the parameter value to `MockedClient.IsRandomBroken`
> - Add another test method and name it `TestGetNonRandomQuote` (don't forget to stick a `[Fact]` attribute on it for the test runner to find it)
> - Write the test code so that is uses the new `getClient` boolean parameter to get non-random quotes. Also, ensure it expected the test to be non-random.

This is what mine looked like:

```csharp
    [Fact]
    public void TestGetNonRandomQuote()
    {
        IQuotifierClient client = getClient(true);
        string previousQuote = null;
        var isRandom = false;
        for (int i = 0; i < 5; i++)
        {
            string randomQuote = client.GetRandomQuote();
            Assert.False(string.IsNullOrEmpty(randomQuote));
            isRandom = previousQuote != null && previousQuote != randomQuote;
            if (isRandom)
                break;
            previousQuote = randomQuote;
        }
        Assert.False(isRandom);
    }
```

As you save the new test the Studio will run the live test in the background (from now on I am just going to assume you are running Live Unit testing so if you're not then please use the Test Explorer UI to run and diagnose your tests) and indicate success by painting pretty green check marks on it.

Before we move on, the mocking we just did is fine but there are actually several great and free frameworks out there, such as [Moq](https://github.com/moq/Moq/) or [Rhino Mocks](https://github.com/RhinoMocks/RhinoMocks) that will help you set up mock services and entities with very few lines of code. To use them you are expected to have a good understanding of concepts like dependency injection ("DI"), pragma methods and the IOC design pattern in general. We won't bother with mocking frameworks or DI in this exercise however so let's leave it at that and move on to parameterized tests, or "*theories*" as they are called in xUnit ...

### Parameterized tests ###

Rather than write pretty much the same test twice with just a few small differences, like we just did, you can create a "**theory**" (as opposed to a "**fact**"). Unlike a "**fact**" test test method a "**theory**" test method can accept parameters and you can then specify literal values to be passed in. In our case we could easily have just written the original test method with a boolean to be used to create the mock client and set its behavior (via its `IsRandomBroken` property). Let's do that ...

> - Remove the latest tets method (`TestGetNonRandomQuote`).
> - Replace the `[Fact]` attribute of the original test method (`TestGetRandomQuote`) with a `[Theory]` attribute instead.
> - Add a `bool` parameter to the test method and name it `expectRandom`.
> - Pass the `expectRandom` **negated** as the argument to the `getClient` call:  `getClient(!expectRandom)`.
> - Adjust the final `Assert` and replace the call to `True` with `Equal`. Then set pass `expectRandom` as its first argument and keep the `isRandom` as the second argument: `Assert.Equal(expectRandom, isRandom);`

The end result should look something like this:

```csharp
    [Theory]
    public void TestGetRandomQuote(bool expectRandom)
    {
        IQuotifierClient client = getClient(!expectRandom);
        string previousQuote = null;
        var isRandom = false;
        for (int i = 0; i < 5; i++)
        {
            string randomQuote = client.GetRandomQuote();
            Assert.False(string.IsNullOrEmpty(randomQuote));
            isRandom = previousQuote != null && previousQuote != randomQuote;
            if (isRandom)
                break;
             previousQuote = randomQuote;
        }
        Assert.Equal(expectRandom, isRandom);
    }
```

We now just have to pass the two possible values of `expectRandom` to test the outcome, using the same test code. You pass literal values with another method attribute: `[InlineData(<literal value>)]`, like so:

```csharp
    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void TestGetRandomQuote(bool expectRandom)
    {
        IQuotifierClient client = getClient(!expectRandom);
        string previousQuote = null;
        var isRandom = false;
        for (int i = 0; i < 5; i++)
        {
            string randomQuote = client.GetRandomQuote();
            Assert.False(string.IsNullOrEmpty(randomQuote));
            isRandom = previousQuote != null && previousQuote != randomQuote;
            if (isRandom)
                break;
            previousQuote = randomQuote;
        }
        Assert.Equal(expectRandom, isRandom);
    }
```

Give the Studio a few moments to recode and run the test. When the dust settles the test should be green as a spring meadow.

## Parallelism ##

*This section will introduce asynchronous coding to developers that have never really had to bother with the concept. If you feel you are already experienced with parallelism, either generally or with .NET specifically, then I recommend you either skip ahead or just quickly browse through the section.* 

Before we plug in the mocked client we need to take a step back and talk about a crucial piece of technology that you will need to master to be a successful developer: Parallel coding (or just "parallelism").

The client we just wrote is executing in the same thread as the code that called it. That's not how we want to do things in a mobile app, actually. Ask yourself: What will happen in the actual code, when it calls a remote service to get a random quote? As the client makes the call, possibly when there are some networking issues? What you do **not want** is for the user interface to get unresponsive while the user stares at the "**(please wait ...)**" text and waits for her quote to show up. What you probably **do want** is for the app to accept desperate taps at the "Refresh" button or, in the future, when you have continued to develop **Quotifier** with more features (such as the ability to submit your own quotes or share quotes with friends on Facebook), is for the user to navigate away from the random quote view and do something else while she waits for the random quote to be refreshed.

You therefore want the `IQuotifierClient.GetRandomQuote()` method to be **asynchronous**, meaning it will be executed in a different (background) thread and then you want to be notified when there is a new random quote available. When that happens you then want to assign it to the view model's `RandomQuote` property and the view to be refreshed (which happens automagically as the Xamarin.Forms binding mechanism gets to work).

So, to summarize, we need to modify the `IQuotifierClient.GetRandomQuote()` method to make it **asynchronous** and then we need to modify the code that calls it to make it react to when the method is done, without blocking its running thread.

There are many ways to make a method work in a background thread, depending on which version of .NET your targeting. Back in the day you would have to ...

1. Write a small class called "**GetRandomQuoteAsync**" (or something similar) to deal with the asynchronous call.
2. Add an event to that class and name it to "**Done**" or something to that effect. 
3. Probably add another property called "**Error**" to contain any exceptions thrown in the background thread as those would never surface to the main (UI) thread otherwise.
4. Add a third property of type ´string´ and call it "**Result**" (probably). 
5. Add a method called "**RunAsync**" (or something similar), to be invoked when you wanted a new random quote.
6. Add an event handler to the **Done** event that would deal with error handling (by checking the **Error** property) and assign the view model's `RandomQuote` property. Also, in that method you might (depending on UI framework) have to ensure the assignment happens in the main (UI) thread.
7. Call the **RunAsync** method
8. Realize you need to do pretty much all of the above for every service you need to call and get a result from.
9. Praise [Anders Hejlsberg](https://en.wikipedia.org/wiki/Anders_Hejlsberg) and all the other geniuses in the .NET crew for adding generic types to you favorite programming language.
10. Rewrite the above class to make it generic, like `Work<T>`, `RunInBackground` or `Task<T>`.
11. Improve your generic background-running class to automatically throw exceptions in the main thread (if that's the behavior you wanted).

Having done the above you would be in a pretty good place to be able to easily (-ish) write parallel code and great apps that never leaves the user muttering four-letter words while staring at a frozen screen. 

Just to give you a sense of life "back in the day", here's some code to illustrate the idea. I will spare you the gritty details of the pre-generic type era and just show what a (somewhat naïve) generic asynchronous call handler could look like: 

```csharp
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    namespace Me.Parallelism
    {
        public class Work<T>
        {
            ManualResetEvent _semaphore = new ManualResetEvent(false);
            DoneHandler _doneHandler;

            public Work<T> Done(DoneHandler doneHandler)
            {
                _doneHandler = doneHandler;
                return this;
            }

            public void RunAsync(Func<T> code, TimeSpan? waitTimeSpan)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var result = code();
                        if (_doneHandler != null)
                            _doneHandler(this, new DoneEventArgs(result));
                    }
                    catch (Exception error)
                    {
                        if (_doneHandler != null)
                            _doneHandler(this, new DoneEventArgs(error));
                    }
                    _semaphore.Set();
                });
                if (waitTimeSpan.HasValue && waitTimeSpan != TimeSpan.MinValue)
                    _semaphore.WaitOne(waitTimeSpan.Value);
            }

            public class DoneEventArgs : EventArgs
            {
                public T Result { get; }
                public Exception Error { get; }

                public DoneEventArgs(T result)
                {
                    Result = result;
                }

                public DoneEventArgs(Exception error)
                {
                    Error = error;
                }
            }

            public delegate void DoneHandler(object sender, DoneEventArgs args);
        }
    }
```

If you are unfamiliar with parallel coding and concepts such as *semaphores* then go ahead and create a small unit test or Console project, copy/paste the above code and try it out. Here's two unit tests to get you started:

```csharp
    public class TestWork
    {
        [Fact]
        public void TestWorkResult()
        {
            new Work<string>()
                .Done((s, e) =>
                {
                    Assert.Null(e.Error);
                    Assert.Equal("Hello World!", e.Result);
                })
                .RunAsync(() => 
                {
                    return "Hello World!";
                }, 
                TimeSpan.FromSeconds(1));
        }

        [Fact]
        public void TestWorkException()
        {
            new Work<string>()
                .Done((s, e) =>
                {
                    Assert.Null(e.Result);
                    Assert.NotNull(e.Error);
                    Assert.Equal("Good Bye Cruel World!", e.Error.Message);
                })
                .RunAsync(() =>
                {
                    throw new Exception("Good Bye Cruel World!");
                },
                TimeSpan.FromSeconds(1));
        }
    }
```

Simply put, the semaphore (of class `ManualResetEvent`) is just a flag (`bool`) that can be either set (`true`) or unset (`false`) and provided the semaphore is available to multiple threads you can use it to synchronize them. In the above example it is being used to by the client code (the test), which is executing in the main thread, to wait for the background thread to finish. I implemented it so that it wait for a set period of time (`TimeSpan`) rather than indefinitely. That us usually more useful in most scenarios.

Anyway, the C# programming language is designed with productivity in mind and there is a lot of "compiler sugar" to avoid having to write lines and lines of more or less the same code to solve typical problems. To solve common asynchronous scenarios, where code needs to be called from one thread and run in a different thread, and where the calling code, at some point, do need to wait for a result; two new keywords where introduced in C# 4.0: **`async`** and  **`await`**.

Simply put the **`async`** keyword can be supplied as part of a method declaration that return a `Task` or a `Task<T>`. The keyword signals to the compiler that it should expect one or more **`await`**s in the code body. The **`await`** keyword (requires that the method is **`async`**) is where the magic happens.  Simply put, what happens is that all code following the **`await`** keyword will be run in a background thread when the call to the (*awaited*) asynchronous code has finished. The two kewords can be challenging to get your head around at first (and there's quite a few coders out there that has misunderstood them): Common misconception that are often nurtured is that **`await`** blocks the thread while waiting for the call to finish. It's not. Instead, consider this code:

```csharp
    public class TestAsyncAwait
    {
        public void Run()
        {
            Console.WriteLine($"Before the call (thread #{Thread.CurrentThread.ManagedThreadId})");
            calculate(1, 2);
            Console.WriteLine($"After the call (thread #{Thread.CurrentThread.ManagedThreadId})");
        }

        public async void calculate(int a, int b)
        {
            var sum = await AddAsync(a, b);
            Console.WriteLine($"Sum={sum} (thread #{Thread.CurrentThread.ManagedThreadId})");
        }

        public Task<int> AddAsync(int a, int b)
        {
            return Task.Factory.StartNew<int>(() =>
            {
                Console.WriteLine($"Calculating ... (thread #{Thread.CurrentThread.ManagedThreadId})");
                Thread.Sleep(2000);
                return a + b;
            });
        }
    }
```

>
> [OPTIONAL]
>
> You might want to try and step through the above code using the debugger If so, the best choice is create a new Console project and run it from there:
>
> - Create a new Console project (File > New > Project > Visual C# > Windows Classic Desktop > Console App (.NET Framework) 
> - Cut and paste the above code, either to a new file (called "**AsyncTimeout.cs**") or into the "**Program.cs**" file.
> - Run the test from the `Program.Main` method:

```csharp
    class Program
    {
        static void Main(string[] args)
        {
            var test = new TestAsyncAwait();
            test.Run();
            Console.ReadLine();
        }
    }
```

Check the Console output:

    Before the call (thread #1)
    After the call (thread #1)
    Calculating ... (thread #3)
    Sum=3 (thread #3)

The first two lines will show instantly while the third line (`Sum=3`) takes two seconds to appear due to the simulated delay in the background thread performing the calculation `Thread.Sleep(2000);`. This proves that the code following the call to the asynchronous `AddAsync` is indeed being run in the background thread after the invoked code completed. The code that invoked the asynchronous code did not block at the **await** keyword. Instead it left the method and returned to write "**After the call ...**" to the console (before returning to the main method and wait for the user to press `ENTER`). Only when the `calculate` method completed back there, in the background, was the next line (`Console.WriteLine($"Sum=...`) executed. 

For better understanding you can track how the program is switching between threads you can set a breakpoint at the start of the Main method, then start the console app and go: Debug > Windows > Threads. This will open a new pane that will help you keep track of which thread the currently executing code line belongs to. It should be pretty straight forward in this little program but can be a challenging exercise in more complex applications.       

> - Take some time to step through the code a few time to understand the concept.

### An asynchronous client ###

Armed with our new knowledge about how to utilize multiple threads to perform potentially time-consuming tasks we will now improve the `IQuotifierClient` and make the `GetRandomQuote` method asynchronous. Changing the declaration is as simple as replacing the return value (`string`) with a generic task containing a `string` result:

```csharp
    interface IQuotifierClient
    {
        Task<string> GetRandomQuote();
    }
```

> - Change the `IQuotifierClient.GetRandomQuote` method declaration (see above code).
> - Navigate to the `MockedClient` and modify the method declaration to implement the interface. Then modify the code to make it run in a background `Task` and to return the `Task`:

```csharp
    public Task<string> GetRandomQuote()
    {
        return Task.Factory.StartNew(() 
            => IsRandomBroken
                ? _quotes[0]
                : _quotes[_random.Next(5)]);
    }
```

> - Now navigate to the unit test (`TestQuotifierClient.TestGetRandomQuote`) and make it **`async`** and to **`await`** the result from `GetRandomQuote`:

```csharp
    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public async void TestGetRandomQuote(bool expectRandom) // <-- now async
    {
        IQuotifierClient client = getClient(!expectRandom);
        string previousQuote = null;
        var isRandom = false;
        for (int i = 0; i < 5; i++)
        {
            string randomQuote = await client.GetRandomQuote(); // <-- NOW await:ing result
            Assert.False(string.IsNullOrEmpty(randomQuote));
            isRandom = previousQuote != null && previousQuote != randomQuote;
            if (isRandom)
                break;
            previousQuote = randomQuote;
        }
        Assert.Equal(expectRandom, isRandom);
    }
```

> - Run the unit test and make sure it still passes.

### Let's try the app's behavior ###

Now for the fun part: To see how the app behaves with a mocked client.  

> - Go back to the Android and add a reference to the **Quotifier.Client** project
> - Repeat for the iOS project
> - Navigate to the `RandomQuoteVM.refresh` method and modify it to fetch a `IQuotifierClient` and return whatever it gets back from the client's `GetRandomQuote` method:

![Mocked client](images/RandomQuoteVM.refresh.png)

> - Utilize the suggested quick-fix to generate the missing `getClient` method and make it return a `MockedClient`
> - In the `RandomQuoteVM` ctor; call the `refresh` method to fetch a random quote

Let's see if it works ...

> - Run the app

![Mocked client](images/doneMocking.png)

> - Tap the "Refresh" button. You will (hopefully) get a new random quote from the five we're mocking. If not; try again (remember: our mocked client picks a random quote from just five available quotes so, statistically, there's a 20% chance you will see the same quote again when you tap "*Refresh**").  

## Networking ##

This fulfills the first user story of our backlog but the second one requires a central service so our app must connect to the Quotifier backend.

As we will now connect to and communicate with a RESTful web API there are a few things we need to understand and master:

1. When we request a random quote we will not receive the actual quote. Instead the quote is a property of an object, serialized as JSON (text). The JSON will have to be deserialized into a real POCO (Plain Old CLR Object) object. We will therefore have to provide a class for the JSON to be deserialized into.
2. It might sometimes not work. (Networks are non-deterministic beasts.)  

For the first part there are excellent libraries out there that allows JSON serialization. The most popular one -- by far -- is Newtonsoft.Json, which is available as a NuGet package. We will download this package and see how we can use it for our needs.

We will also have to examine the JSON we get from the web API and see how we can generate our own `Quote` class. 

And, of course, communication with the backend can suffer all kinds of failures depending on the quality of the network. After all, your code is targeted at a computer that is being carried around between cellular networks, into Wifi and back again. Switching network is normal for a mobile device and some of he networks can be shaky. Bottom line: we need to add some error handling and/or come up with strategies to protect the user experience as best we can.

### Exploring the API ###

Before we start deserializing quotes we should explore the API we are about to communicate with.  

> - If the Quotifier.WebAPI solution is not already open in another instance of Visual Studio; open a new Visual Studio instance in elevated mode and load the solution 
> - Ensure the Quotifier.WebAPI.ConsoleHost project is set as the start-up project and run it

![Web API started in console](images/webApiStarted-console.png)

Please note that the IP number in the console you started will be different than what you see in the screenie. 

> - Make a mental (or actual) note of the IP based URL ("http://192.168.2.69:80" in my example - yours will be different). You will need it later.
> - Open a web browser and navigate to that address (or to [http://localhost:80](http://localhost:80); for web browsers it won't matter).

![Web API start page](images/webApi-startPage.png)

That web page is just the self-hosted API's home page of course and we will need to slap something more to that URL to get to the actual services we need. Typically a web api service endpoint can be reached by adding "/api" followed by a "/" and the name of the service. The problem, of course, is knowing the name of the service. In this example there is a home page with useful links but this will not be the case for most APIs, which we didn't write ourselves. That will rarely be the case for APIs that are hosted in the cloud.     

This is where Swagger comes to our rescue. The service can generate a Swagger OpenAPI document which can then be used to explore the various services offered by the API. The Quotifier service is one of those services and you can reach its Swagger UI by just adding "/swagger" to the base URL. 

> - Click the [/swagger link](http://localhost:80/swagger).
>   You will be automatically redirected to the UI endpoint, which lists two services: **NotificationInstallations** and the one we came looking for - **Quotes**
> - Click **Quotes** to explore the API

![Swagger UI](images/azureHost-swaggerUI-01.png)

The API offers not only random quotes (`GET /api/quotes/random`) but also all available quotes (`GET /api/quotes`), individual quotes, when you submit the quote id (`GET /api/quotes/{id}`), and so on. You can also create new quotes (`POST /api/quotes`), update (`PUT /api/quotes`) or delete a quote (`DELETE /api/quotes/{id}`). To try any of the services out just click it to expand a small UI then click the **Try it out** button in that UI. 

*It's ok to deleted, update and so on. The quotes "database" will be recreated when you restart the API*

> - Click the `GET /api/quotes` service.
> - Click the `Try now` button.
> - If necessary; scroll down the the "Response Body" to see the JSON response.
>
> If you are unfamiliar with JSON then please [take some time to read up on the format](<http://json.org/>). 
>
> - Open the `GET /api/quotes/random` service - the one we are looking to use in our mobile app - and click **Try it out**.

![Web API random quote](images/webApi-swagger-randomQuote.png)

> - Please make a mental (or actual) note of the service endpoint: **/api/quotes/random**. That is what we will have to add to the base URL in out mobile app soon.

The JSON we get back from this request represents an object that, among other things, contains an "**Id**", an "**Original**" value (the quote), a "**Translated**" value and, in this case, "**Categories**" which is an array (qualified by the "**[**" prefix and "**]**" suffix) containing two more objects of some other (nameless) type. JSON contains no type names but from this information we should be able to write our own `Quote` and `Category` classes. You can do this either manually or you can use some ready-made tool to parse the JSON and emit C# code for it. There are several such tools of course. One of them can be found at <http://json2csharp.com/>. To use it just copy the JSON from the response and paste it into the tool's big text box and click "**Generate**".

> - Create a "**Models**" folder in the **Quotifier.Client** project. 
> - Use the <http://json2csharp.com/> to generate model classes `Quote` and `Category`. Put the classes into individual files in the "**Models**" folder you just added.

### Serialization with Newtonsoft.Json ###

Now that we have a model we can consume JSON objects and have them deserialized into the actual model objects we just created (`Quote` and `Category`). The ambitious coder will of course roll her own JSON serializer, parser and deserializer but that will be (far) outside the scope for this lab! Instead we will rely on the most popular NuGet packet out there: Newtonsoft.Json. To use it we need to download it to all app projects.

> - Add the Newtonsoft.Json NuGet package to the Quotifier.Android project
> - Add the Newtonsoft.Json NuGet package to the Quotifier.iOS project
> - (**optional**) Familiarize yourself with Newtonsoft.Json library ([here's the home page, including a documentation link](<https://www.newtonsoft.com/json>)).

### Consuming the service ###

It is time to add the last component - a working client - to be integrated with the app to get a random quote from the RESTful web API. This is a new feature of our app so, as always, we should drive its development from tests. Now, testing a new unit that relies on some other unit always complicates things and should be avoided if possible but, alas, in reality it is rarely possible. 

In unit tests we do not want non-deterministic behavior and, so, we have to try and control the testing environment completely. Relying on a remote service that we wrote and manage ourselves makes this very challenging and, in the end, network fluctuations will make the unit testing non-deterministic anyway. Relying on some proprietary service, provided by an external party makes testing the dependent unit impossible. So, obviously, we now need to come up with a strategy to simulate the network traffic.

*Before we continue I would like to point out that the we will rely on the `HttpClient` class for network communication. If now is the first time you see this class then you should consider [reading up a bit](https://developer.xamarin.com/api/type/System.Net.Http.HttpClient/) (and maybe do a few spikes) before continuing. I will present all necessary code from here on so you can just copy it. But for real understanding it is of course always better to get your hands dirty by using the stuff.*

We have already written a good unit test for our client and there is no good reason the new web-client should not be tested the same way. But the test is currently only testing the `MockedClient` class, which is being instantiated and returned by the `getClient` method. We now have two options:

1. Write an (almost) identical unit test and a variant of `getClient` to return an instance of `WebAPIClient` instead.
2. Expand the parameters for the existing unit test to include testing the `WebAPIClient`

There might be good arguments for both options but, for now, let's go with option two.  

> - Open the unit test file (`TestGetRandomQuote.cs`)
> - Add another `bool` parameter and name it "**mockedClient**".
>
> Another test parameter must of course be reflected in the `InlineData` we pass in ...
>
> - Add two more `InlineData` attributes and make the (now) four attribute reflect all combinations of the two `bool` values
> - Pass the new "**mockedClient**" value as a second parameter to the `getClient` method and adjust it accordingly.
> - Modify the code body of `getClient` so that it returns the `MockedClient` (like before) when the `mockedClient` is set (`true`) or a `WebAPIClient` otherwise.
> - Pass a URL (`string` literal) and a new instance of a `MockedHttpMessageHandler` as arguments to the `WebAPIClient` instance. The `MockedHttpMessageHandler` ctor should be passed the same `bool` value controlling whether random quotes is expected or not (`isRandomBroken`). 

The plan now is to "hijack" the network traffic so the URL can be anything. The screenie passes "<http://quotifier.azurewebsites.net>" (which is our Quotifier API published in Azure by the way) but that endpoint will never be reached anyway.
The unit test should now look like this:

![Unit test WebAPIClient](images/unitTestWebApiClient.png)

So, like oftentimes when we write unit tests we are introducing new (non-existing) types. We will fix this like we did before ...

> - Use the Studio's Quick Action options to generate the new types in new files.
> - Open the `WebAPIClient` and rename the first ctor "**baseAddress**" and "**messageHandler**".
> - Also, rename the fields to match.
> - Use the Studio's Quick Action to implement the interface. 

The solution should now be in shape to build again. Reflecting on the TDD flow (write test > see them fail > implement behavior(s) > ensure tests pass > refactor > start over) we are now on the second step. Let's continue ...

> - When the solution builds; run the test and assert it fails.
> - Implement the `WebAPIClient` to get a random quote (as JSON) from the remote service (found `/api/quotes/random` at the `_baseAddress`). Then just throw an `NotImplementedException` error to highlight we need to provide more code. For now, this should be enough and then we'll look at the deserialization and the final piece of code.  

Here's a suggested implementation:

```csharp
    public class WebAPIClient : IQuotifierClient
    {
        readonly string _baseAddress;
        readonly HttpMessageHandler _messageHandler;

        public async Task<string> GetRandomQuote()
        {
            using (var client = newHttpClient())
            {
                var responseMessage = await client.GetAsync("/api/quotes/random");
                if (responseMessage.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Request failed");

                var json = await responseMessage.Content.ReadAsStringAsync();

                throw new NotImplementedException();
            }
        }

        HttpClient newHttpClient()
        {
            var client = new HttpClient(_messageHandler) { BaseAddress = new Uri(_baseAddress) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public WebAPIClient(string baseAddress = "http://quotifier.azurewebsites.net", HttpMessageHandler messageHandler = null)
        {
            if (string.IsNullOrWhiteSpace(baseAddress)) throw new ArgumentNullException(nameof(baseAddress));
            _baseAddress = baseAddress;
            _messageHandler = messageHandler;
        }
    }
```

> - Move (mouse-drag) the `WebAPIClient` type to the `Quotifier.Client` project. (Delete the file in your test project.)

Before we can test the new `WebAPIClient` we need to "hijack" the network traffic. This can be done by providing our own `HttpMessageHandler`. The trick here is to make the `MockedHttpMessageHandler` a "proper" `HttpMessageHandler` that can be recognized by the `HttpClient` we're using in `WebAPIClient` and then override the (abstract) `SendAsync` method to intercept the request and send back a response, without ever making any remote calls. 

> - Open the `MockedHttpMessageHandler.cs`
> - Make the class inherit from `HttpMessageHandler`
> - Use the Studio's Quick Action to implement the (abstract) `SendAsync` method. 
>   Hint: You can use an instance of the `MockedClient` to get and return a random quote as JSON.

Here's a suggested implementation if you're too lazy to roll your own :-) ...

```csharp
    class MockedHttpMessageHandler : HttpMessageHandler
    {
        readonly MockedClient _mockedService;

        public virtual HttpResponseMessage Send(HttpRequestMessage request)
        {
            if (request.Method == HttpMethod.Get && request.RequestUri.AbsolutePath == "/api/quotes/random")
            {
                var randomQuote = new Quote
                {
                    Original = _mockedService.GetRandomQuote().Result
                };
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(JsonConvert.SerializeObject(randomQuote))
                };
            }
            throw new NotImplementedException();
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(Send(request));
        }

        public MockedHttpMessageHandler(bool isRandomBroken)
        {
            _mockedService = new MockedClient() { IsRandomBroken = isRandomBroken };
        }
    }
```

> - Run the test. 

There should now effectively be four tests (driven by the four `InlineData` attributes). Two of these should pass (the ones using the `MockedClient`) and two should fail (the ones testing the `WebAPIClient`). On closer inspection the failure is due to our code throwing an `NotImplementedException`, just as expected. Excellent!

If the code isn't crystal clear, go ahead and step through it a couple of times using the debugger too see how it works.

The final part of implementing the web client is to just deserialize the result and return the value found in the resulting model object's "**Original**" property.

> - Return to the `WebAPIClient` and remove the line throwing a `NotImplementedException` 
> - Deserialize the JSON you got back, into a `Quote` object.
> - Return the `Quote` object's "**Original**" property value.

Here's the final implementation of the `WebAPIClient`:

```csharp
    public class WebAPIClient : IQuotifierClient
    {
        readonly string _baseAddress;
        readonly HttpMessageHandler _messageHandler;

        public async Task<string> GetRandomQuote()
        {
            using (var client = newHttpClient())
            {
                var responseMessage = await client.GetAsync("/api/quotes/random");
                if (responseMessage.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Request failed");

                var json = await responseMessage.Content.ReadAsStringAsync();
                var quote = JsonConvert.DeserializeObject<Quote>(json);
                return quote.Original;
            }
        }

        HttpClient newHttpClient()
        {
            var client = new HttpClient(_messageHandler) { BaseAddress = new Uri(_baseAddress) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public WebAPIClient(string baseAddress = "http://quotifier.azurewebsites.net", HttpMessageHandler messageHandler = null)
        {
            if (string.IsNullOrWhiteSpace(baseAddress)) throw new ArgumentNullException(nameof(baseAddress));
            _baseAddress = baseAddress;
            _messageHandler = messageHandler;
        }
    }
```

> - Run the test again

All four tests should now pass!

## Let's do it for real ##

To make the mobile apps consume an actual remote service is now easy: Just modify the view model's `getClient` method to return an instance of the `WebAPIClient`. Please note that in the suggested implementation of that class we are automatically passing the URL <http://quotifier.azurewebsites.net>, which is where I have published the Quotifier API for you to try it out. If you prefer running the desktop-hosted version of the API then just flip over to another instance of Visual Studio (make sure it's running in elevated mode), load the **Quotifier.WebAPI** solution and start the console edition of the API. Then copy one of the IP-based addresses it binds to and use that URL as an argument for your `WebAPIClient` instead.

*Please note that when you run from a mobile device, whether an simulator, emulator or physical device, they cannot reach your API through the <http://localhost:80> URL! The emulators are running in their own virtual machine (VM) so "localhost" is simply referring to their own execution environment, not Windows.*

> - Run the iOS and Android apps, either on physical devices or in a simulator/emulator.

Please note that there might be network related issues.

## Done! Now what? ##

Looking at our little backlog ...

> ***USER STORY 1 / Random quote :***
>
> *As a user I need random quotes when I want them so I can reflect on their wisdom (or be amused).*
>
> ***USER STORY 2 - Random quote*** :
>
> *As the product owner I need a central service to be available in the cloud, so that I can build more tools and add features without having to redeploy or provide my own infrastructure*

The stories are done and to realize this backlog we have covered all of the following topics:

- Creating cross-platform solutions in Visual Studio
- Shared libraries
- The MVVM design pattern
- XAML
- Databinding
- Debugging apps in emulators and physical devices, Android and iOS
- Connecting to the Max Xamarin agent
- Xamarin layouts, Labels and Buttons
- Refactoring (renaming)
- Conditional (platform-specific) XAML
- Test Driven Development
- Mocking
- Asynchronous coding (a.k.a. parallelism)
- Networking (`HttpClient`and `HttpMessageHandler`)

Looking back, this introduction was more about cross-cutting concerns than plain Xamarin. The reason for this is that building Xamarin Forms apps is quite easy once you have gotten the basics (which you now have). After that it is mainly a matter of checking our more UI elements and getting some experience from using them. I have provided several links, most of them to the <http://developer.xamarin.com> domain. In that domain you will find loads of valuable guides, code recipes and examples for how to use other Xamarin Forms layouts and  controls. You can also expand on your knowledge about the native APIs of Android and iOS.

I now recommend you continue to expand on the Quotifier app and improve it. You can experiment more with beautifying it and expanding its feature set. Here's a few suggestions to get you going:

- Experiment more with fonts, margins etc.
- Add a ["wait" animation](https://developer.xamarin.com/api/type/Xamarin.Forms.ActivityIndicator/) when refreshing
- Improve the status bar (at the top of the screen) so that it's hidden during the launch screen and then presented with white text ("light" style) when the main page shows up.
- Add more pages and a navigation pattern. The pages can allow submitting your own quotes and editing existing ones.
- Add authentication

Allow your imagination and ambition free reign. 

Have fun!

/Jonas