﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Quotifier.Models;

namespace Quotifier.WebAPI.Controllers
{
    [Route("api/quotes")]
    public class QuotesController : ApiController
    {
        static readonly Lazy<IQuoteRepository> _s_quoteRepo = new Lazy<IQuoteRepository>(() => new QuotesRepository());

        static IQuoteRepository QuoteRepo => _s_quoteRepo.Value; 

        [Authorize]
        [HttpGet, Route("api/quotes")]
        public IHttpActionResult GetQuotes() => Ok(QuoteRepo.GetAll());

        [HttpGet, Route("api/quotes/{id}")]
        public Quote GetQuote(string id) => QuoteRepo.Get(id);

        [HttpGet, Route("api/quotes/random")]
        public Quote GetRandom() => QuoteRepo.GetRandom();

        [HttpGet, Route("api/quotes/categories")]
        public IEnumerable<string> GetCategories() => QuoteRepo.GetCategories();

        [HttpGet, Route("api/quotes/category/{id}")]
        public IEnumerable<Quote> GetCategory(string id) => QuoteRepo.GetCategory(id);

        [HttpGet, Route("api/quotes/category/{id}/random")]
        public Quote GetRandomFromCategory(string id) => QuoteRepo.GetrandomFromCategory(id);

        [HttpGet, Route("api/quotes/count")]
        public IHttpActionResult GetCount() => Ok(QuoteRepo.Count);

        [HttpPost]
        public IHttpActionResult Create([FromBody]Quote quote)
        {
            try
            {
                if (quote == null || !ModelState.IsValid)
                    return BadRequest("Invalid State");

                QuoteRepo.Add(quote);
            }
            catch (Exception ex)
            {
                return BadRequest($"Error while creating: {ex}");
            }
            return Ok(quote);
        }

        [HttpPut]
        public IHttpActionResult Put([FromBody] Quote quote)
        {
            try
            {
                if (quote == null || !ModelState.IsValid)
                {
                    return BadRequest("Invalid State");
                }
                QuoteRepo.Update(quote);
            }
            catch (Exception)
            {
                return BadRequest("Error while creating");
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete, Route("api/quotes/{id}")]
        public IHttpActionResult Delete(string id)
        {
            try
            {
                QuoteRepo.Remove(id);
                return StatusCode(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
