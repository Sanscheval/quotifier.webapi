﻿using System;

namespace Quotifier.Models
{
    public class QuoteCategory
    {
        private const string TagQualifier = @" :: ";

        public string Key { get; set; }

        public string Value { get; set; }

        public override string ToString() => $"{Value}{(string.IsNullOrEmpty(Key) ? "" : $" {TagQualifier}{Key}")}";

        public static implicit operator QuoteCategory(string s) => Parse(s);

        public static implicit operator string(QuoteCategory category) => category.ToString();

        public string ToLowerInvariant() => ToString().ToLowerInvariant();

        #region .  Equality  .
        public static bool operator ==(QuoteCategory cat1, QuoteCategory cat2)
        {
            if (ReferenceEquals(null, cat1)) return ReferenceEquals(null, cat2);
            return cat1.Equals(cat2);
        }

        public static bool operator !=(QuoteCategory cat1, QuoteCategory cat2) => !(cat1 == cat2);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuoteCategory)obj);
        }

        public bool Equals(QuoteCategory category, StringComparison stringComparison = StringComparison.Ordinal)
        {
            if (category == null) return false;
            if (stringComparison == StringComparison.Ordinal) return GetHashCode() == category.GetHashCode();
            if (!string.IsNullOrEmpty(Key))
            {
                if (string.IsNullOrEmpty(category.Key)) return false;
                return Key.Equals(category.Key, stringComparison) && Value.Equals(category.Value, stringComparison);
            }
            if (!string.IsNullOrEmpty(category.Key)) return false;
            return Key.Equals(category.Key, stringComparison) && Value.Equals(category.Value, stringComparison);
        }

        protected bool Equals(QuoteCategory other)
        {
            return string.Equals(Key, other?.Key) && string.Equals(Value, other?.Value);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Key?.GetHashCode() ?? 0) * 397) ^ (Value?.GetHashCode() ?? 0);
            }
        }

        #endregion 

        public static QuoteCategory Parse(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;
            var tagAt = s.IndexOf(TagQualifier, StringComparison.Ordinal);
            if (tagAt == -1) return new QuoteCategory(s, null);
            var tag = s.Substring(tagAt + TagQualifier.Length);
            s = s.Substring(0, tagAt);
            return new QuoteCategory(s, tag);
        }

        public QuoteCategory(string value, string tag = null)
        {
            Value = value.Trim();
            Key = tag?.Trim();
        }

        public QuoteCategory()
        {
        }
    }
}
