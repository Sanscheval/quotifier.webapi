﻿using System;
using Newtonsoft.Json;

namespace Quotifier.Models
{
    public class Quote
    {
#if DEBUG
        private static int _s_debugId;
        [JsonIgnore]
        public int DebugId { get; } = ++_s_debugId;
#endif

        private int _hashCode;
        private Guid _id;

        public Guid Id
        {
            get { return _id; }
            set
            {
                _id = value;
                _hashCode = 0;
            }
        }

        public string Original { get; set; }
        public string Translated { get; set; }
        public QuoteCategory[] Categories { get; set; }

        public Quote()
        {
            Id = Guid.NewGuid();
        }

        #region .  Equality  .
        public override int GetHashCode()
        {
            if (_hashCode != 0) return _hashCode;
            _hashCode = Id.GetHashCode();
            return _hashCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is Quote q && Equals(q);
        }

        public bool Equals(Quote quote) => quote?.GetHashCode() == GetHashCode();

        public static bool operator ==(Quote q1, Quote q2)
        {
            if (ReferenceEquals(null, q1)) return ReferenceEquals(null, q2);
            return !ReferenceEquals(null, q2) && q1.Equals(q2);
        }

        public static bool operator !=(Quote q1, Quote q2)
        {
            return !(q1 == q2);
        }
        #endregion
    }
}
