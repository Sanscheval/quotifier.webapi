﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Quotifier.Models
{
    public class QuotesFileDatastore : IQuotesDatastore
    {
        public void Create(params Quote[] quotes)
            => Update(quotes);

        public void Update(params Quote[] quotes)
        {
            var existingQuotes = new HashSet<Quote>(LoadAll());
            foreach (var quote in quotes)
            {
                existingQuotes.Remove(quote);
                existingQuotes.Add(quote);
            }
            Write(existingQuotes);
        }

        public void Delete(params Quote[] quotes)
        {
            var existingQuotes = new HashSet<Quote>(LoadAll());
            foreach (var quote in quotes)
            {
                if (existingQuotes.Contains(quote))
                    existingQuotes.Remove(quote);
            }
            Write(existingQuotes);
        }

        public IEnumerable<Quote> LoadAll()
        {
            var path = getDataPath();
            if (!path.Exists)
                return new Quote[0];

            var json = File.ReadAllText(path.FullName);
            return JsonConvert.DeserializeObject<IEnumerable<Quote>>(json);
        }

        public void Write(IEnumerable<Quote> quotes)
        {
            var path = getDataPath();
            if (!string.IsNullOrEmpty(path.DirectoryName) && !Directory.Exists(path.DirectoryName))
                Directory.CreateDirectory(path.DirectoryName);

            var json = JsonConvert.SerializeObject(quotes);
            File.WriteAllText(path.FullName, json);
        }

        static FileInfo getDataPath()
        {
            return new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), ".data", "quotes.json"));
        }
    }
}