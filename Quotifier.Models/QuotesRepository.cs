﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Quotifier.Models
{
    public class QuotesRepository : IQuoteRepository
    {
        HashSet<Quote> _quotes = new HashSet<Quote>();
        readonly Dictionary<string, HashSet<Quote>> _categoryIndex = new Dictionary<string, HashSet<Quote>>();
        readonly Random _rnd = new Random(DateTime.Now.Millisecond);
        private readonly IQuotesDatastore _datastore;

        public int Count => _quotes.Count;

        public void Add(Quote quote)
        {
            _quotes.Add(quote);
            indexOnCategories(quote);
            _datastore.Create(quote);
        }

        public void Update(Quote quote)
        {
            var previousQuote = _quotes.FirstOrDefault(q => q.Equals(quote));
            if (previousQuote != null)
            {
                indexOnCategories(quote);
                removeObsoleteCategoriesFromIndex(previousQuote, quote);
                _datastore.Update(quote);
                _quotes.Remove(quote);
                _quotes.Add(quote);
            }
        }

        private void removeObsoleteCategoriesFromIndex(Quote previousQuote, Quote newQuote)
        {
            foreach (var cat in previousQuote.Categories)
            {
                if (!newQuote.Categories.Any(c => c.Equals(cat, StringComparison.OrdinalIgnoreCase)))
                    _categoryIndex[cat].Remove(previousQuote);
            }
        }

        public Quote Remove(string key)
        {
            if (!Guid.TryParse(key, out var guid))
                return null;

            var existingQuote = _quotes.FirstOrDefault(q => q.Id == guid);
            if (existingQuote == null)
                return null;

            removeFromCategoryIndex(existingQuote);
            _datastore.Delete(existingQuote);
            _quotes.Remove(existingQuote);
            return existingQuote;
        }

        private void removeFromCategoryIndex(Quote quote)
        {
            foreach (var category in quote.Categories)
            {
                var key = category.ToLowerInvariant();
                _categoryIndex[key].Remove(quote);
            }
        }

        public Quote Get(string id)
        {
            if (!Guid.TryParse(id, out var guid))
                throw new FormatException($"Invalid id: {id}");

            return _quotes.FirstOrDefault(q => q.Id == guid);
        }

        public IEnumerable<Quote> GetCategory(string category)
        {
            var key = category.ToLowerInvariant();
            if (_categoryIndex.TryGetValue(key, out var quotes))
                return quotes;

            return new Quote[0];
        }

        public Quote GetrandomFromCategory(string category)
        {
            var all = GetCategory(category).ToList();
            var at = _rnd.Next(all.Count);
            if (at < 0) return null;
            var lookup = 0;
            return all.FirstOrDefault(quote => lookup++ == at);
        }

        public IEnumerable<string> GetCategories() => _categoryIndex.Keys;

        public QuotesRepository Load()
        {
            // todo Separate into DAL
            var quotes = _datastore.LoadAll();
            _quotes = new HashSet<Quote>(quotes);
            buildCategoryIndex();
            return this;
        }

        private void buildCategoryIndex()
        {
            _categoryIndex.Clear();
            foreach (var quote in _quotes)
                indexOnCategories(quote);
        }

        void indexOnCategories(Quote quote)
        {
            foreach (var category in quote.Categories)
            {
                var key = category.ToLowerInvariant();
                if (!_categoryIndex.TryGetValue(key, out var quotes))
                {
                    quotes = new HashSet<Quote> { quote };
                    _categoryIndex.Add(key, quotes);
                    continue;
                }
                quotes.Add(quote);
            }
        }

        public IEnumerable<Quote> GetAll() => _quotes;

        public Quote GetRandom()
        {
            var at = _rnd.Next(_quotes.Count);
            if (at < 0) return null;
            var lookup = 0;
            return _quotes.FirstOrDefault(quote => lookup++ == at);
        }

        public QuotesRepository(IQuotesDatastore datastore)
        {
            _datastore = datastore ?? new QuotesFileDatastore();
            Load();
        }
    }
}
