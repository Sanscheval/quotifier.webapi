﻿using System.Collections.Generic;

namespace Quotifier.Models
{
    public interface IQuotesDatastore
    {
        IEnumerable<Quote> LoadAll();
        void Create(params Quote[] quotes);
        void Update(params Quote[] quotes);
        void Delete(params Quote[] quotes);
    }
}