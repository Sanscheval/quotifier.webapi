﻿using System.Collections.Generic;

namespace Quotifier.Models
{
    public interface IQuoteRepository
    {
        void Add(Quote quote);
        void Update(Quote quote);
        Quote Remove(string key);
        Quote Get(string id);
        IEnumerable<Quote> GetAll();
        IEnumerable<Quote> GetCategory(string category);
        Quote GetRandom();
        IEnumerable<string> GetCategories();
        Quote GetrandomFromCategory(string category);
        int Count { get; }
    }
}
