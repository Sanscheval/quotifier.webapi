﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;

namespace Quotifier.WebAPI.OAuthServerProvider
{
    // https://www.codeproject.com/articles/876867/asp-net-web-api-understanding-owin-katana-authenti
    class ApplicationOAuthServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) 
            => await Task.FromResult(context.Validated());

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            await Task.Factory.StartNew(() =>
            {
                // todo Consider actually checking credentials
                if (context.Password != "password")
                {
                    context.SetError("invalid_grant", "The user name of password is incorrect.");
                    context.Rejected();
                    return;
                }

                // create or retrieve a ClaimsIdentity to represent the authenticated user ...
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("user_name", context.UserName));

                // identity will ultimately be encoded into an Access Token as a result of this call ...
                context.Validated(identity);
            });
        }
    }
}
