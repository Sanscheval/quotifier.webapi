﻿using System;
using System.Text;

namespace Quotifier.WebAPI
{
    static class StringHelper
    {
        public static string TrimVerbatimLines(this string s, char lineStart = '^', string newLine = null)
        {
            if (string.IsNullOrEmpty(s)) return s;
            newLine = newLine ?? Environment.NewLine;
            var ca = s.ToCharArray();
            var trim = true;
            var sb = new StringBuilder();
            for (var i = 0; i < ca.Length; i++)
            {
                if (isNewLine())
                {
                    trim = true;
                    sb.Append(newLine);
                    i += newLine.Length - 1;
                    continue;
                }
                if (ca[i] == lineStart)
                {
                    trim = false;
                    continue;
                }
                if (!trim) 
                    sb.Append(ca[i]);

                bool isNewLine()
                {
                    if (ca[i] != newLine[0]) return false;
                    if (i + newLine.Length > ca.Length-1) return false;
                    for (var j = 1; j < newLine.Length; j++)
                    {
                        if (ca[i + j] != newLine[j]) return false;
                    }
                    return true;
                }
            }
            return sb.ToString();
        }

        public static string EnsurePrefix(this string s, string prefix)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentNullException(nameof(s));
            if (string.IsNullOrEmpty(prefix)) throw new ArgumentNullException(nameof(prefix));
            return prefix.StartsWith(prefix) ? s : $"{prefix}{s}";
        }

    }
}
