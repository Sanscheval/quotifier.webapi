﻿using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Quotifier.WebAPI.OAuthServerProvider;
using Swashbuckle.Application;

namespace Quotifier.WebAPI
{
    class Startup
    {
        public static EmbeddedOAuthAuthorizationProviderOptions EmbeddedOAuthAuthorizationProvider
        { get; } = EmbeddedOAuthAuthorizationProviderOptions.Default;

        public void Configuration(IAppBuilder app)
        {
            app.Use(async (owin, next) =>
            {
                Console.WriteLine($"HTTP method: {owin.Request.Method}, path: {owin.Request.Path}");
                await next();
                Console.WriteLine($"Response code: {owin.Response.StatusCode}");
            });
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.EnableSwagger(x => x.SingleApiVersion("v1", "Self-hosted Quotifier Web API")).EnableSwaggerUi();
            if (EmbeddedOAuthAuthorizationProvider.IsEnabled)
                configureEmbeddedOAuthAuthorizationProvider(app);

            app.UseWebApi(config);
            app.Run(owin => owin.Response.WriteAsync(@"
                <html>
                <body>
                    <h2>Quotifier Web API</h2>
                    <p>Api: <a href=""./api/quotes"">/api/quotes</a></p>
                    <p>Explore: <a href=""./swagger"">/swagger</a></b></p>
                </body>
                </html>"));
        }

        private void configureEmbeddedOAuthAuthorizationProvider(IAppBuilder app)
        {
            Console.WriteLine($"Supported: OAuth STS ({(EmbeddedOAuthAuthorizationProvider.AllowInsecureHttp ? "SSL=OFF" : "SSL=ON")})");
            var options = EmbeddedOAuthAuthorizationProvider;
            var oauthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString(options.TokenEndpoint),
                Provider = new ApplicationOAuthServerProvider(),
                AccessTokenExpireTimeSpan = options.AccessTokenLongevity,
                AllowInsecureHttp = options.AllowInsecureHttp
            };
            app.UseOAuthAuthorizationServer(oauthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }

    public class EmbeddedOAuthAuthorizationProviderOptions
    {
        private string _tokenEndpoint;
        public bool IsEnabled { get; set; }
        public TimeSpan AccessTokenLongevity { get; set; }
        public bool AllowInsecureHttp { get; set; }

        public string TokenEndpoint
        {
            get => _tokenEndpoint;
            set
            {
                if (string.IsNullOrWhiteSpace(value)) throw new ArgumentNullException(nameof(value));
                _tokenEndpoint = value.EnsurePrefix("/");
            }
        }

        public static EmbeddedOAuthAuthorizationProviderOptions Default => new EmbeddedOAuthAuthorizationProviderOptions
            {
                IsEnabled = false,
                TokenEndpoint = "/Token",
                AccessTokenLongevity = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };

        private EmbeddedOAuthAuthorizationProviderOptions()
        {
        }
    }
}
