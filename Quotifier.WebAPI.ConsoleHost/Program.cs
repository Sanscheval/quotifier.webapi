﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Microsoft.Owin.Hosting;

namespace Quotifier.WebAPI
{
    class Program
    {
        const string Port = "80";
        const string Uri = "http://{host}:{port}";

        static void Main(string[] args)
        {
            var allIps = getAllMachineIpAddresses();
            var localhostUrl = Uri.Replace("{host}", "localhost").Replace("{port}", Port);
            var listeners = new List<IDisposable>();

            Startup.EmbeddedOAuthAuthorizationProvider.IsEnabled = true;

            using (startListening(localhostUrl))
            {
                foreach (var ipAddress in allIps.Where(nic => nic.AddressFamily == AddressFamily.InterNetwork))
                {
                    var ip = ipAddress.ToString();
                    var ipUrl = Uri.Replace("{host}", ip).Replace("{port}", Port);
                    var listener = startListening(ipUrl);
                    listeners.Add(listener);
                }
                string cmd;
                do
                {
                    cmd = Console.ReadLine();
                }
                while (cmd?.Equals("quit", StringComparison.InvariantCultureIgnoreCase) ?? false);
                foreach (var listener in listeners)
                {
                    listener.Dispose();
                }
            }
        }

        private static IDisposable startListening(string url)
        {
            try
            {
                Console.WriteLine($"Listening on: {url} ...");
                return WebApp.Start<Startup>(url);
            }
            catch (Exception ex)
            {
                if (isAccessDenied(ex))
                    Console.WriteLine($@"
                        ^ACCESS ERROR! When starting listening to {url}.
                        ^   Consider running in elevated mode or create a URL reservation, like this (using cmd) ...
                        ^   netsh http add urlacl url=http://+:7990/ user=<domain>\<your user id>
                        ^   ... if that doesn't help then specify the ip address ...
                        ^   netsh http add urlacl url=http://<ap address>:7990/ user=<domain>\<your user id>"
                        .TrimVerbatimLines('^'));
                else
                    Console.WriteLine($"ERROR! When starting listening to {url}: ", ex);
                return new DisposableDummy();
            }

            bool isAccessDenied(Exception ex)
            {
                var httpError = ex.InnerException as HttpListenerException;
                return httpError?.ErrorCode == 5;
            }
        }

        static string getLocalIpAddress(bool throwOnFail = true)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            var localIp = host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
            if (localIp != null)
                return localIp.ToString();

            if (throwOnFail)
                throw new Exception("Local IP Address Not Found!");

            return null;
        }

        static IPAddress[] getAllMachineIpAddresses(bool throwOnFail = true)
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            return Dns.GetHostAddresses(Dns.GetHostName());  
        }

        class DisposableDummy : IDisposable
        {
            public void Dispose() {}
        }
    }
}
