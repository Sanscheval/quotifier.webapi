﻿using System.Web.Http;
using Owin;
using Quotifier.WebAPI.ErrorHandling;

namespace Quotifier.WebAPI
{
    partial class Startup // inserts middlerware to catch unhandled errors
	{
        private static void configureUnhandledError(IAppBuilder app, HttpConfiguration config)
        {
            app.Use<UnhandledExceptionMiddleware>();
        }
    }
}