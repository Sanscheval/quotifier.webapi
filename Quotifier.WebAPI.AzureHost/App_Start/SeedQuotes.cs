﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Quotifier.Models;

namespace Quotifier.WebAPI
{
    public static class SeedQuotes
    {
        #region .  Quotes as JSON  .
        private const string Json = @"
[
  {
    ""Id"": ""63ccb7c1-daa6-4609-9a3b-044d8ef0fddf"",
    ""Original"": ""Every portrait that is painted with feeling is a portrait of the artist, not of the sitter."",
    ""Translated"": ""Varje porträtt som målats med känsla föreställer konstnären, inte modellen."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""ef04095a-3d6e-4f9c-9840-08e2d3eeff66"",
    ""Original"": ""From now on, ending a sentence with a preposition is something up with which I will not put."",
    ""Translated"": ""Att avsluta en mening med en preposition är något jag utmed längre icke står."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      }
    ]
  },
  {
    ""Id"": ""8bc3fc9e-a567-4f55-bcef-163845517922"",
    ""Original"": ""It's not enough that we do our best; sometimes we have to do what's required."",
    ""Translated"": ""Det räcker inte att vi gör vårt bästa; ibland måste vi göra vad som krävs."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""35091187-be34-462c-96d0-18d76252415d"",
    ""Original"": ""However beautiful the strategy, you should occasionally look at the results."",
    ""Translated"": ""Oavsett skönheten i din strategi bör du understundom granska resultaten."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      }
    ]
  },
  {
    ""Id"": ""d95b6868-70ba-4db3-885a-19020ea407f9"",
    ""Original"": ""Men occasionally stumble over the truth, but most of them pick themselves up and hurry off as if nothing ever happened."",
    ""Translated"": ""Vi snubblar alla över sanningen ibland, men de flesta skyndar vidare och låtsas inte om det."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""a53f9de1-b8c0-4f11-bcc6-1bb3a779762c"",
    ""Original"": ""Anyone who lives within their means suffers from a lack of imagination."",
    ""Translated"": ""Alla som rättar munnen efter massäcken lider av bristande fantasi."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""63e97975-5271-40b2-9b51-278bc957da97"",
    ""Original"": ""Genius is born--not paid."",
    ""Translated"": ""Genialitet föds; det kan ej köpas."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""ac163a3f-81de-4719-970e-2c8212119d89"",
    ""Original"": ""I have always felt that a politician is to be judged by the animosities he excites among his opponents."",
    ""Translated"": ""Jag har alltid ansett att en politiker bäst bedöms efter fientligheten han skapar hos sina motståndare."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      }
    ]
  },
  {
    ""Id"": ""7012aa9c-ac50-497f-9661-3a10f3433497"",
    ""Original"": ""I like pigs. Dogs look up to us. Cats look down on us. Pigs treat us as equals."",
    ""Translated"": ""Jag gillar grisar. Hundar ser upp till oss. Katter ser ned på oss. Grisar behandlar oss som sina likar."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      }
    ]
  },
  {
    ""Id"": ""143134bf-f376-4132-8422-49eb56ec4a89"",
    ""Original"": ""Consistency is the last refuge of the unimaginative."",
    ""Translated"": ""Konsistens är den fantasilöses sista tillflykt."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""b48f0b68-b6d7-428e-b1a5-4ecc086871a8"",
    ""Original"": ""It has been said that democracy is the worst form of government except all the others that have been tried."",
    ""Translated"": ""Det har sagts att demokrati är den värsta formen av styre, bortsett från alla andra som har prövats."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""cc7e45a6-0cd5-4d2b-bcf2-565f3c45a931"",
    ""Original"": ""Veni vidi vici"",
    ""Translated"": ""Tärningen är kastad"",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""Latin""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Ceasar""
      }
    ]
  },
  {
    ""Id"": ""427dd329-aeca-4f5b-bb58-5d3818c21bd6"",
    ""Original"": ""Fashion is a form of ugliness so intolerable that we have to alter it every six months."",
    ""Translated"": ""Mode är sådan styggelse för ögat att den måste ändras varje halvår."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""ff8bcdcd-017f-4be7-a4f6-5dcc57b8187c"",
    ""Original"": ""Women are made to be loved, not understood"",
    ""Translated"": ""Kvinnor ska älskas, inte förstås"",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language :: language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""051ad575-2b99-4a98-9077-5f1f5042abcc"",
    ""Original"": ""History will be kind to me for I intend to write it."",
    ""Translated"": ""Historien kommer tala väl om mig eftersom jag tänker skriva den själv."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""95e6b85a-aa04-4865-b7e8-6d94e3fe17e9"",
    ""Original"": ""A man can be happy with any woman as long as he does not love her."",
    ""Translated"": ""En man kan vara lycklig med alla kvinnor han inte älskar."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language :: language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""11f9334a-82f8-4296-9e80-7f378a8a0fdb"",
    ""Original"": ""Illusion is the first of all pleasures."",
    ""Translated"": ""Villfarlse är den främsta av alla lustar."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""0d3b3739-9d35-4d1f-a5f3-80c620dfd7c8"",
    ""Original"": ""Arguments are to be avoided; they are always vulgar and often convincing."",
    ""Translated"": ""Undvik åsikter; de är alltid vulgära och oftast övertygande."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""7a7c7f6e-cad6-4b26-925f-a99036cb0443"",
    ""Original"": ""America had often been discovered before Columbus, but it had always been hushed up."",
    ""Translated"": ""Amerika upptäckes flera gånger före Columbus, men det tystades alltid ned."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""413f06fc-e2ad-4e46-82e0-b3ddc8dfcfb0"",
    ""Original"": ""America is the only country that went from barbarism to decadence without civilization in between."",
    ""Translated"": ""Amerika är det enda landet som gick från barbarism till dekadens utan att passera civilisationsstadiet."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""d7b545b5-ecb8-4306-9897-bc95a4d9f300"",
    ""Original"": ""A man is very apt to complain of the ingratitude of those who have risen far above him."",
    ""Translated"": ""Alla är bra på att beklaga otacksamheten hos alla som varit mer lyckosamma."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""468939d0-7178-4d37-b29b-bf913127768a"",
    ""Original"": ""It is a very sad thing that nowadays there is so little useless information."",
    ""Translated"": ""Det är tråkigt att så litet meningslös information återstår idag."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""3a2d4e57-19b2-4d45-8903-c645cdaf5d47"",
    ""Original"": ""I was working on the proof of one of my poems all the morning, and took out a comma. In the afternoon I put it back again."",
    ""Translated"": ""Jag ägnade hela morgonen åt korrektur och avlägsnade ett komma. Under eftermiddagen återställde jag det."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""021ab3bd-f0c7-4604-a001-ce7da9ff37f6"",
    ""Original"": ""It is a mistake to try to look too far ahead. The chain of destiny can only be grasped one link at a time."",
    ""Translated"": ""Det är dumt att söka se för långt fram. Ödets kedja kan endast förstås en länk i taget."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Winston Churchill""
      }
    ]
  },
  {
    ""Id"": ""d56e76bb-3030-4ced-b375-dd612ba333ff"",
    ""Original"": ""Always forgive your enemies; nothing annoys them so much."",
    ""Translated"": ""Förlåt alltid dina fiender; inget irriterar dem mer."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""79a936e0-c3cd-4cb7-865d-e12e60ea62c1"",
    ""Original"": ""Navigare necesse est"",
    ""Translated"": ""Det är nödvändigt att segla"",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Gnaeus Pompejus""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""Latin""
      }
    ]
  },
  {
    ""Id"": ""342e0f74-fa9a-4392-aed9-e73c4be0ad94"",
    ""Original"": ""I always like to know everything about my new friends, and nothing about my old ones."",
    ""Translated"": ""Jag vill alltid veta allt om mina nya vänner, och inget om mina gamla."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""47a5b17e-6daf-4b2c-8c5a-e757a41cb907"",
    ""Original"": ""I think that God in creating Man somewhat overestimated his ability."",
    ""Translated"": ""Jag tror att när Gud skapade människan överskattade han sin förmåga."",
    ""Categories"": [
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      },
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      }
    ]
  },
  {
    ""Id"": ""4dc4bc3b-8657-4eb7-9695-f1111c68c6ea"",
    ""Original"": ""If you want to tell people the truth, make them laugh, otherwise they'll kill you."",
    ""Translated"": ""Berätta alltid sanningen som ett skämt, annars blir du snart dödad av dina åhörare."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""84b5cd08-c5ad-4b1f-9bb4-fa1598269521"",
    ""Original"": ""I am not young enough to know everything."",
    ""Translated"": ""Jag är icke ung nog att veta allt."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Oscar Wilde""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  },
  {
    ""Id"": ""1cb6d209-9818-44dd-8aaf-3e69ade8ed80"",
    ""Original"": ""Men and nations behave wisely when they have exhausted all other resources."",
    ""Translated"": ""Människor och nationer uppträder klokt när alla andra alternativ prövats."",
    ""Categories"": [
      {
        ""Key"": ""author"",
        ""Value"": ""Abba Eban""
      },
      {
        ""Key"": ""language"",
        ""Value"": ""English""
      }
    ]
  }
]
";
        #endregion

        public static IEnumerable<Quote> Quotes
        {
            get
            {
                var result = JsonConvert.DeserializeObject<IEnumerable<Quote>>(Json);
                return result;
            }
        } 
    }
}