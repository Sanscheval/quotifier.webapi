﻿using System.Collections.Generic;
using System.Data.Entity;
using AutoMapper;
using Quotifier.WebAPI.DataObjects;
using Quotifier.WebAPI.Models;

namespace Quotifier.WebAPI
{
    public class MobileServiceInitializer : CreateDatabaseIfNotExists<MobileServiceContext>
    {
        protected override void Seed(MobileServiceContext context)
        {
            var eQuotes = Mapper.Map<IEnumerable<EQuote>>(SeedQuotes.Quotes);
            foreach (var quote in eQuotes)
            {
                context.Set<EQuote>().Add(quote);
            }
            base.Seed(context);
        }
    }
}