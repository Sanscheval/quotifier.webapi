﻿using System.Web.Http;
using Owin;
using Swashbuckle.Application;

namespace Quotifier.WebAPI
{
	public partial class Startup // Swagger UI
	{
	    private void configureSwagger(IAppBuilder app, HttpConfiguration config)
	    {
	        config
				.EnableSwagger(x => x.SingleApiVersion("v1", "Azure hosted Quotifier Web API"))
				.EnableSwaggerUi();
        }
    }
}