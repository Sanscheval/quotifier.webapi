﻿using AutoMapper;
using Quotifier.Models;
using Quotifier.WebAPI.DataObjects;

namespace Quotifier.WebAPI
{
	public partial class Startup
	{
	    private void configureAutomapper()
	    {
			Mapper.Initialize(cfg =>
			{
			    cfg.CreateMap<EQuoteCategory, QuoteCategory>().ReverseMap();
			    cfg.CreateMap<EQuote, Quote>().ReverseMap();
			});
	    }
	}
}