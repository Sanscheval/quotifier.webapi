﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quotifier.WebAPI.DataObjects
{
    public class EQuote
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Original { get; set; }
        public string Translated { get; set; }
        public List<EQuoteCategory> Categories { get; set; } 

        public EQuote()
        {
            Categories = new List<EQuoteCategory>();
        }
    }
}