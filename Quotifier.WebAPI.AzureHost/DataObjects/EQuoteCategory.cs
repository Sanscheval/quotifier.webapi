﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quotifier.WebAPI.DataObjects
{
    public class EQuoteCategory
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Value { get; set; }
        public string Key { get; set; }
        public List<EQuote> Quotes { get; set; }

        public EQuoteCategory()
        {
            Quotes = new List<EQuote>();
            Id = Guid.NewGuid();
        }
    }
}