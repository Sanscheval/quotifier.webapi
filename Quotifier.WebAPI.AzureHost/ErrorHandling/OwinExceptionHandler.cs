﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace Quotifier.WebAPI.ErrorHandling
{
    public class OwinExceptionHandler : ExceptionHandler
    {
        public override async Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            var info = System.Runtime.ExceptionServices.ExceptionDispatchInfo.Capture(context.Exception);
            info.Throw();
        }
    }
}