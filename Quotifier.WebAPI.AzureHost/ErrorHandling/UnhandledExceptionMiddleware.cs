﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Owin;

namespace Quotifier.WebAPI.ErrorHandling
{
    public class UnhandledExceptionMiddleware : OwinMiddleware
    {
        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                await Next.Invoke(context);
                if (context.Response.StatusCode == 500)
                    dumpServerError(context);
            }
            catch (Exception ex)
            {
                dumpUnhandledException(ex, context);
                throw;
            }
        }

        private void dumpServerError(IOwinContext context)
        {
            var sb = new StringBuilder();
            sb.AppendLine("{");
            sb.AppendLine($"\tstatusCode: \"{context.Response.StatusCode}\"");
            sb.AppendLine($"\treasonPhrase: \"{context.Response.ReasonPhrase}\"");
            if (context.Response.Body.CanRead)
                using (TextReader reader = new StreamReader(context.Response.Body))
                {
                    var body = reader.ReadToEnd();
                    sb.AppendLine($"\tbody: \"{reader.ReadToEnd()}\"");
                }
            sb.AppendLine("}");
            File.WriteAllText(getErrorFilename(".servererror", context), sb.ToString());
        }

        private void dumpUnhandledException(Exception ex, IOwinContext context)
        {
            File.WriteAllText(getErrorFilename(".unhandlederror", context), ex.ToString());
        }

        string getErrorFilename(string suffix, IOwinContext context)
        {
            var appData = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data");
            //var root = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            var file = new FileInfo(Path.Combine(appData, "errors", $"{DateTime.UtcNow.ToString("yyyyMMddhhmmssff")}{suffix}"));
            if (!Directory.Exists(file.Directory.FullName))
                Directory.CreateDirectory(file.Directory.FullName);
            return file.FullName;
        }

        public UnhandledExceptionMiddleware(OwinMiddleware next) : base(next)
        {
        }
    }
}