﻿using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Quotifier.WebAPI;
using System.Web.Http.ExceptionHandling;
using Quotifier.WebAPI.ErrorHandling;

[assembly: OwinStartup(typeof(Startup))]

namespace Quotifier.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.Services.Replace(typeof(IExceptionHandler), new OwinExceptionHandler());
            configureUnhandledError(app, config);
            config.MapHttpAttributeRoutes();
            configureAutomapper();
            configureMobileApp(app, config);
            configureSwagger(app, config);
        }
    }
}