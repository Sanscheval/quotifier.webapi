using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server.Tables;
using Quotifier.WebAPI.DataObjects;

//using Quotifier.WebAPI.AzureHost.DataObjects;

namespace Quotifier.WebAPI.Models
{
    public class MobileServiceContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
        //
        // To enable Entity Framework migrations in the cloud, please ensure that the 
        // service name, set by the 'MS_MobileServiceName' AppSettings in the local 
        // Web.config, is the same as the service name when hosted in Azure.

        private const string ConnectionStringName = "Name=MS_TableConnectionString";

        public DbSet<EQuote> Quotes { get; set; }
        public DbSet<EQuoteCategory> QuoteCategories { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; } // obsolete

        public MobileServiceContext() : base(ConnectionStringName)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));

            modelBuilder.Entity<EQuote>()
                .HasMany(q => q.Categories)
                .WithMany(c => c.Quotes);
        }
    }
}
