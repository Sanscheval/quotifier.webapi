﻿using System.Collections.Generic;
using System.Linq;
using Quotifier.Models;
using System.Data.Entity;
using AutoMapper;
using Quotifier.WebAPI.DataObjects;

namespace Quotifier.WebAPI.Models
{
    public class QuotesEntityDatastore : IQuotesDatastore
    {
        public IEnumerable<Quote> LoadAll()
        {
            using (var db = new MobileServiceContext())
            {
                var eQuotes = db.Quotes.Include(quote => quote.Categories).ToList();
                return Mapper.Map<IEnumerable<Quote>>(eQuotes);
            }
        }

        public void Create(params Quote[] quotes)
        {
            var eQuotes = Mapper.Map<IEnumerable<EQuote>>(quotes);
            using (var db = new MobileServiceContext())
            {
                db.Quotes.AddRange(eQuotes);
                db.SaveChanges();
            }
        }

        public void Update(params Quote[] quotes)
        {
            var eQuotes = Mapper.Map<IEnumerable<EQuote>>(quotes);
            using (var db = new MobileServiceContext())
            {
                foreach (var eQuote in eQuotes)
                {
                    db.Entry(eQuote).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public void Delete(params Quote[] quotes)
        {
            var eQuotes = Mapper.Map<IEnumerable<EQuote>>(quotes);
            using (var db = new MobileServiceContext())
            {
                foreach (var eQuote in eQuotes)
                {
                    db.Entry(eQuote).State = EntityState.Deleted;
                }
                db.SaveChanges();
            }
        }
    }
}